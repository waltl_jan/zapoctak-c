#ifndef COLLISION_SOLVERS_OCTREE_SOLVER_HEADER
#define COLLISION_SOLVERS_OCTREE_SOLVER_HEADER

#include "CollisionSolver.hpp"
#include "OctreeSolver/Octree.hpp"
#include "Source/Geometry/Sphere.hpp"
#include <cstdint>
#include <functional>

namespace collSim
{
	bool Contains(const Sphere& s, const AABB& cube);
	bool Intersect(const Sphere& l, const Sphere& r);
	bool Intersect(const Sphere& l, const AABB& cube);
	AABB GetAABB(const Sphere& s);
	void ResolveSpheres(Sphere& l, Sphere& r);

	//Uses Octree structure to resolve collisions more effectively
	//TODO switch on/off the containing cube
	class OctreeSolver :public CollisionSolver
	{
	public:
		using statsCallback_t = std::function<void(const OctreeStats&)>;
		OctreeSolver(std::size_t numSpheres,std::size_t splitLimit,double minSize, double maxSize, bool collCube);
		virtual void Resolve(double deltaT)override final;

		Octree<Sphere>& GetTree()
		{
			return octree;
		}
		//Function that can collects stats
		//Is called each time Resolve is called
		void SetStatsCallback(statsCallback_t func)
		{
			statsCallback = func;
		}
	private:
		void CollWithCube(Sphere& sphere);
		bool collCube;
		Octree<Sphere> octree;
		statsCallback_t statsCallback = nullptr;
	};
}

#endif