#ifndef COLLISION_SOLVERS_COLLISION_SOLVER_HEADER
#define COLLISION_SOLVERS_COLLISION_SOLVER_HEADER


#include "Source/Geometry/Sphere.hpp"

namespace collSim
{
	class CollisionSolver
	{
	public:
		virtual void Resolve(double deltaT) = 0;
	};
}


#endif