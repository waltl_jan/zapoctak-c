#ifndef OCTREE_SOLVER_COMMON_TYPES_HEADER
#define OCTREE_SOLVER_COMMON_TYPES_HEADER


#include <cstdint>
#include <limits>
#include "OctreeCollection.hpp"

namespace collSim
{
	template<typename Object> class Node;
	template<typename Object>
	using nodesCollection = OctreeCollection<Node<Object>>;
	template<typename Object>
	using nodesCollIT = typename nodesCollection<Object>::iterator;

	//Wrapper around an object that remembers in which octree node it's currently placed.
	template<typename Object>
	struct ObjectWrapper
	{
		ObjectWrapper(nodesCollIT<Object> parent, Object&& object) :parent(parent), object(std::move(object)) {}
		Object object;
		void SetParent(nodesCollIT<Object> newParent)
		{
			parent = newParent;
		}
		nodesCollIT<Object> GetParent() const
		{
			return parent;
		}
	private:
		nodesCollIT<Object> parent;
	};

	template<typename Object>
	using objCollection = OctreeCollection<ObjectWrapper<Object>>;
	template<typename Object>
	using objCollIT = typename objCollection<Object>::iterator;
	template<typename Object>
	using objCollCIT = typename objCollection<Object>::const_iterator;
}


#endif