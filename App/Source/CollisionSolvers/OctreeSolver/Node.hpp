#ifndef OCTREE_SOLVER_NODE_HEADER
#define OCTREE_SOLVER_NODE_HEADER

#include <vector>
#include <array>
#include <cstdint>
#include <type_traits>
#include <optional>

#include "Source/Geometry/AABB.hpp"
#include "CommonTypes.hpp"
#include "OctreeCollection.hpp"

//TODO comment this file properly
namespace collSim
{
	template<typename Object>
	//Represents a node of the Octree
	class Node
	{
	public:
		Node(const AABB& cube, std::optional<nodesCollIT<Object>> parent = std::optional<nodesCollIT<Object>>{}) :
			cube(cube), parent(parent) {}
		Node(const Node&) = delete;
		Node(Node&&) = default;
		Node& operator=(const Node&) = delete;
		Node& operator=(Node&&) = default;

		bool HasChildren() const;
		//childIndex=[0,8)
		bool HasChild(size_t childIndex) const;
		//childIndex=[0,8)
		void AddChild(size_t childIndex, nodesCollIT<Object> childNode);
		void ChangeChild(size_t childIndex, nodesCollIT<Object> newChild);
		void RemoveChild(size_t childIndex);
		//childIndex=[0,8)
		// returns noIndex if there's no child
		nodesCollIT<Object> GetChild(size_t childIndex) const;
		void SetParent(nodesCollIT<Object> newParent)
		{
			assert(&*this != &(*newParent));
			parent = newParent;
		}
		std::optional<nodesCollIT<Object>> GetParent() const
		{
			return parent;
		}

		//Position and size of this node
		AABB cube;
		//Objects stored at this node
		std::vector<typename objCollection<Object>::iterator> objects;
		//childIndex=[0,8)
		static Vec3d GetChildOffset(size_t childIndex);
		static std::size_t GetChildIndex(const Vec3d& vec);

		friend void swap(Node<Object>& left, Node<Object>& right)
		{
			using std::swap;
			swap(left.cube, right.cube);
			swap(left.parent, right.parent);
			swap(left.objects, right.objects);
			swap(left.children, right.children);
		}
	private:
		std::optional<nodesCollIT<Object>> parent;
		std::array<nodesCollIT<Object>, 8> children;
	};

	template<typename Object>
	bool Node<Object>::HasChildren()const
	{
		return std::any_of(children.begin(), children.end(), [](auto&& it) {return it; });
	}

	template<typename Object>
	bool Node<Object>::HasChild(size_t child)const
	{
		assert(child < 8);
		return children[child];
	}
	template<typename Object>
	void Node<Object>::AddChild(size_t child, nodesCollIT<Object> childNode)
	{
		assert(child < 8);
		assert(!HasChild(child));
		children[child] = childNode;
	}
	template<typename Object>
	void Node<Object>::ChangeChild(size_t childIndex, nodesCollIT<Object> newChild)
	{
		assert(childIndex < 8);
		children[childIndex] = newChild;
	}
	template<typename Object>
	void Node<Object>::RemoveChild(size_t childIndex)
	{
		children[childIndex] = nodesCollIT<Object>();
	}
	template<typename Object>
	nodesCollIT<Object> Node<Object>::GetChild(size_t child) const
	{
		assert(child < 8);
		return children[child];
	}
	template<typename Object>
	Vec3d Node<Object>::GetChildOffset(size_t childIndex)
	{
		assert(childIndex < 8);

		switch (childIndex)
		{
		case 0:
			return Vec3d(-1.0, 1.0, 1.0);//Left, Top, Front
		case 1:
			return Vec3d(1.0, 1.0, 1.0);//Right, Top, Front
		case 2:
			return Vec3d(1.0, 1.0, -1.0);//Right, Top, Back
		case 3:
			return Vec3d(-1.0, 1.0, -1.0);//Left, Top, Back
		case 4:
			return Vec3d(-1.0, -1.0, 1.0);//Left, Bottom, Front
		case 5:
			return Vec3d(1.0, -1.0, 1.0);//Right, Bottom, Front
		case 6:
			return Vec3d(1.0, -1.0, -1.0);//Right, Bottom, Back
		case 7:
			return Vec3d(-1.0, -1.0, -1.0);//Left, Bottom, Back
		default:
			assert(0);
			return Vec3d();
		}
	}
	//Must have nonzero elements
	template<typename Object>
	std::size_t Node<Object>::GetChildIndex(const Vec3d& vec)
	{
		if (vec.x > 0.0)
			if (vec.y > 0.0)
				if (vec.z > 0)
					return 1;
				else
					return 2;
			else
				if (vec.z > 0)
					return 5;
				else
					return 6;
		else
			if (vec.y > 0.0)
				if (vec.z > 0.0)
					return 0;
				else
					return 3;
			else
				if (vec.z > 0.0)
					return 4;
				else
					return 7;
	}
}


#endif