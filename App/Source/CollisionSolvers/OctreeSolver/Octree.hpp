#ifndef OCTREE_SOLVER_OCTREE_HEADER
#define OCTREE_SOLVER_OCTREE_HEADER


#include <array>
#include <vector>
#include <optional>
#include <stack>
#include <type_traits>
#include "Source/CollisionSolvers/CollisionSolver.hpp"
#include "Source/Math/Math.hpp"
#include "Source/Geometry/AABB.hpp"

#include "Node.hpp"
#include "CommonTypes.hpp"
#include "OctreeCollection.hpp"
#include <chrono>

namespace collSim
{
	struct OctreeStats
	{
		using clock_t = std::chrono::high_resolution_clock;

		clock_t::duration updateTime = clock_t::duration::zero();
		clock_t::duration pruneTime = clock_t::duration::zero();
		size_t emptyNodes = 0;
		size_t nodes = 0;
		size_t objects = 0;
		size_t maxObjectsInNode = 0;
		size_t removedNodes = 0;
		size_t createdNodes = 0;
		size_t maxDepth = 0;
		size_t collChecks = 0;//Is not filled in UpdateTree
		double avgDepth = 0.0;
		double avgObjectsPerNode = 0.0;
	};
	//Object must overload bool Contains(const Object,const AABB)
	//					   bool Intersect(const Object,const AABB)
	//					   bool Intersect(const Object,const Object)
	//					   AABB GetAABB(const Object)
	//TODO Maybe functors instead of functions, but the templates will be messy...
	//Holds collection of Objects in octree structure which allows fast queries for intersections between them.
	template<typename Object>
	class Octree
	{
	public:
		//Bounding box of the initial root nodeIT.
		//splitLimit=maximum number of objects in one octree node.
		Octree(const AABB& rootSize, size_t splitLimit) :
			splitLimit(splitLimit)
		{
			root = nodes.emplace_back(rootSize);
		}
		//Adds copy of a object to the tree
		//template<typename = std::enable_if_t<std::is_copy_constructible_v<Object>>>
		//void Add(const Object& object)
		//{
		//	Add(Object(object));
		//}
		//Adds passed object to the tree
		void Add(Object&& object);
		//Finds first collision with passed object.
		//Ignores self-collision in case of &o==&otherObject
		//collChecks is number of Intersection calls - either with other Objects or AABBs.
		std::optional<objCollIT<Object>> QueryTree(objCollIT<Object> objIT, size_t*collChecks = nullptr);
		std::optional<objCollIT<Object>> QueryTree(const Object& obj, size_t*collChecks = nullptr);
		std::optional<objCollIT<Object>> QuerySubTree(objCollIT<Object> objIT, size_t*collChecks = nullptr);

		//TODO Maybe Wrapper around collectionIT to allow direct access to Object
		// - maybe just wrapper about pointer to first element of the collection and increment by ObjWrapper size
		objCollection<Object>& GetObjects()
		{
			return objects;
		}
		const objCollection<Object>& GetObjects() const
		{
			return objects;
		}
		const nodesCollection<Object>& GetNodes() const
		{
			return nodes;
		}
		//Rebuild the tree according to updated Objects' positions
		void UpdateTree(OctreeStats* stats = nullptr);
	private:
		//Finds first collision with passed object in given subtree.
		//Ignores self-collision in case of &o==&otherObject
		std::optional<objCollIT<Object>> QueryTree(const Object& o, nodesCollIT<Object> startNode, size_t*collChecks);
		//Deletes empty nodes
		void PruneTree(OctreeStats& stats);
		//Deletes passed node from the Octree
		//If the node has a parent, then 
		//childIndex is index to parent's children array to the node.
		//Otherwise the value is ignored.
		void DeleteNode(nodesCollIT<Object> nodeIT, size_t childIndex);
		//Attemps to push object into any of the nodeIT's children(creates them if necessary).
		//Returns childIT if successfull.
		std::optional<nodesCollIT<Object>> PushDown(const Object& object, nodesCollIT<Object> nodeIT);
		//Returns first nodeIT which fully contains passed object.
		//None if the root does not contain it.
		std::optional<nodesCollIT<Object>> SearchUp(const ObjectWrapper<Object>& o);
		//Expands the tree until it contains passed object.
		void ExpandTree(const Object& o);
		//Updates Object's position in the tree based on its new position in the world
		void Update(objCollIT<Object> objIT);
		//Checks that octree's tree structure is in valid state - nodes are properly linked and each object one node and the node has pointer to it.
		void Validate() const;
		//Nodes and objects are stored lineary for easier iteration over them.
		nodesCollection<Object> nodes;
		objCollection<Object> objects;
		nodesCollIT<Object> root;

		size_t splitLimit;
	};
	template<typename Object>
	void Octree<Object>::Add(Object&& object)
	{
		//Expands the tree if necessary
		ExpandTree(object);
		auto newNodeIT = root;
		while (newNodeIT->HasChildren() || newNodeIT->objects.size() >= splitLimit)
		{
			auto optChildIT = PushDown(object, newNodeIT);
			if (!optChildIT)break;
			newNodeIT = *optChildIT;
		}
		//Place the object into this nodeIT
		auto objIT = objects.emplace_back(newNodeIT, std::move(object));
		newNodeIT->objects.push_back(objIT);
	}

	template<typename Object>
	std::optional<objCollIT<Object>> Octree<Object>::QuerySubTree(objCollIT<Object> objIT, size_t*collChecks)
	{
		return QueryTree(objIT->object, objIT->GetParent(), collChecks);
	}
	template<typename Object>
	std::optional<objCollIT<Object>> Octree<Object>::QueryTree(const Object& obj, size_t*collChecks)
	{
		return QueryTree(obj, root, collChecks);
	}
	template<typename Object>
	std::optional<objCollIT<Object>> Octree<Object>::QueryTree(objCollIT<Object> objIT, size_t*collChecks)
	{
		return QueryTree(objIT->object, root, collChecks);
	}
	template<typename Object>
	std::optional<objCollIT<Object>> Octree<Object>::QueryTree(const Object& o, nodesCollIT<Object> startNode,
		size_t*collChecks)
	{
		if (!collChecks)
		{
			static size_t sinkChecks;
			collChecks = &sinkChecks;
		}

		std::stack<nodesCollIT<Object>> workStack;
		workStack.push(startNode);
		while (!workStack.empty())
		{
			nodesCollIT<Object> node = workStack.top();
			workStack.pop();
			//Intersection with inner objects
			for (const auto& otherIT : node->objects)
			{
				Object& other = otherIT->object;
				++(*collChecks);
				//Returns first non-self intersection
				if (&other != &o && Intersect(other, o))
					return { otherIT };
			}
			//Intersection with subtrees
			for (size_t i = 0; i < 8; ++i)
			{
				if (node->HasChild(i))
				{
					++(*collChecks);
					auto childIT = node->GetChild(i);
					if (Intersect(o, childIT->cube))
						workStack.push(childIT);
				}
			}
		}
		return {};
	}

	template<typename Object>
	void Octree<Object>::UpdateTree(OctreeStats* stats)
	{
		if (!stats)
		{
			static OctreeStats sinkStats;
			stats = &sinkStats;
		}
		stats->nodes = this->nodes.Size();
		stats->objects = this->objects.Size();
		auto beg = OctreeStats::clock_t::now();
		for (auto IT = objects.begin(); IT != objects.end(); ++IT)
			Update(IT);
		stats->updateTime = OctreeStats::clock_t::now() - beg;
		stats->createdNodes = this->nodes.Size() - stats->nodes;
		beg = OctreeStats::clock_t::now();
		if (nodes.Size() > 1)
			PruneTree(*stats);
		stats->pruneTime = OctreeStats::clock_t::now() - beg;
	}

	template<typename Object>
	void Octree<Object>::PruneTree(OctreeStats& stats)
	{
		struct StackNode
		{
			StackNode(nodesCollIT<Object> node, uint8_t childIndex, uint16_t depth) :
				node(std::move(node)), visited(false), childIndex(childIndex), depth(depth) {}
			nodesCollIT<Object> node;

			//Index to parent's children array
			uint16_t depth;
			uint8_t childIndex;
			bool visited;
		};

		//DFS of the Octree, deletes visited nodes on the way up.
		std::stack<StackNode> stack;
		//Max heap sorted by iterator - will ensure that iterators do not invalidate each other
		std::vector<StackNode> toBeDeleted;
		auto cmp = [](StackNode& l, StackNode& r)
		{
			return l.node < r.node;
		};
		auto addChildren = [&stack = stack](nodesCollIT<Object> node, uint16_t depth)
		{
			for (uint8_t i = 0; i < 8; ++i)
				if (node->HasChild(i))
					stack.emplace(node->GetChild(i), i, depth);
		};

		stats.maxDepth = 0;
		stats.maxObjectsInNode = 0;
		stats.emptyNodes = 0;
		uint64_t accumDepth = 0;
		uint64_t accumObjectsPerNode = 0;
		addChildren(root, 1);
		while (!stack.empty())
		{
			StackNode top = stack.top();
			if (!top.visited)//Going down
			{
				stack.top().visited = true;
				auto node = top.node;
				if (node->objects.size() > 0)
				{
					accumDepth += top.depth;
					accumObjectsPerNode += node->objects.size();
					stats.maxObjectsInNode = std::max(stats.maxObjectsInNode, node->objects.size());
					stack.pop();
				}
				else
					++stats.emptyNodes;
				addChildren(node, top.depth + 1);
			}
			else//Going up = children have been visited
			{
				stack.pop();
				stats.maxDepth = std::max(stats.maxDepth, (size_t)top.depth);
				accumDepth += top.depth;
				auto nodeIT = top.node;
				//Postpone the deletion so iterators on the stack do not get invalidated
				if (nodeIT->objects.empty() && !nodeIT->HasChildren())
				{
					toBeDeleted.push_back(std::move(top));

					std::push_heap(toBeDeleted.begin(), toBeDeleted.end(), cmp);
				}
			}
		}
		stats.avgDepth = accumDepth / static_cast<double>(stats.nodes);
		stats.avgObjectsPerNode = accumObjectsPerNode / static_cast<double>(stats.nodes);
		stats.removedNodes = toBeDeleted.size();
		while (toBeDeleted.size() > 0)
		{
			std::pop_heap(toBeDeleted.begin(), toBeDeleted.end(), cmp);
			const StackNode& n = toBeDeleted.back();
			DeleteNode(n.node, n.childIndex);
			toBeDeleted.pop_back();
		}
	}
	template<typename Object>
	void Octree<Object>::DeleteNode(nodesCollIT<Object> nodeIT, size_t childIndex)
	{
		auto parent = nodeIT->GetParent();
		if (parent.has_value())//Has parent = not root
			parent.value()->RemoveChild(childIndex);//Remove its child

		//Relocator = Redirects iterators to movedNode to ensure validity of the Octree
		auto relocator = [this](Node<Object>& movedNode, nodesCollIT<Object> oldPosition, nodesCollIT<Object> newPosition)
		{
			auto movedParent = movedNode.GetParent();
			if (movedParent.has_value())//Redirect its parent
			{
				auto& parent = movedParent.value();
				for (size_t i = 0; i < 8; ++i)
					if (parent->GetChild(i) == oldPosition)
					{
						parent->ChangeChild(i, newPosition);
						break;
					}
			}
			else//Redirect the root
				this->root = newPosition;
			//Redirect its objects
			std::for_each(movedNode.objects.begin(), movedNode.objects.end(),
				[newPosition = newPosition](objCollIT<Object>& o)
			{
				o->SetParent(newPosition);
			});
			//Redirect its children
			for (size_t i = 0; i < 8; ++i)
				if (movedNode.HasChild(i))
					movedNode.GetChild(i)->SetParent(newPosition);
		};
		nodes.Remove(nodeIT, relocator);
	}
	template<typename Object>
	std::optional<nodesCollIT<Object>> Octree<Object>::PushDown(const Object& object, nodesCollIT<Object> nodeIT)
	{
		assert(Contains(object, nodeIT->cube));

		bool inserted = false;
		//Go through children
		for (size_t i = 0; i < 8 && !inserted; ++i)
		{
			if (nodeIT->HasChild(i))//See if the object fits here
			{
				auto childIT = nodeIT->GetChild(i);
				if (Contains(object, childIT->cube))
					return childIT;
			}
			else
			{
				auto cube = AABB(nodeIT->cube.center + nodeIT->cube.halfDist / 2.0 * nodeIT->GetChildOffset(i),
					nodeIT->cube.halfDist / 2.0f);
				if (Contains(object, cube))//Create the child
				{
					auto newNodeIT = nodes.emplace_back(cube, nodeIT);
					assert(nodeIT._index != newNodeIT._index);
					nodeIT->AddChild(i, newNodeIT);
					//THOUGHT Some of the objects in the nodeIT might also want to be pushed down. Do it here? Leave it for update?

					return newNodeIT;
				}
			}
		}
		return {};
	}

	template<typename Object>
	std::optional<nodesCollIT<Object>> Octree<Object>::SearchUp(const ObjectWrapper<Object>& o)
	{
		auto nodeIT = o.GetParent();
		while (!Contains(o.object, nodeIT->cube))
		{
			if (!nodeIT->GetParent())
				return {};
			nodeIT = nodeIT->GetParent().value();
		}
		return nodeIT;
	}

	template<typename Object>
	void Octree<Object>::ExpandTree(const Object& o)
	{
		if (Contains(o, root->cube))
			return;
		auto rootIT = root;
		//Object is outside of the root, keep adding levels until root contains the object
		AABB objCube = GetAABB(o);
		while (!Contains(o, rootIT->cube))
		{
			auto dir = (rootIT->cube.center - objCube.center).Normalize();
			Vec3d rootOffset{
				std::signbit(dir.x) ? +1.0 : -1.0,
				std::signbit(dir.y) ? +1.0 : -1.0,
				std::signbit(dir.z) ? +1.0 : -1.0
			};

			AABB newRootCube{ rootIT->cube.center + rootOffset * rootIT->cube.halfDist,rootIT->cube.halfDist*2.0 };

			rootIT = nodes.emplace_back(newRootCube);
			rootIT->AddChild(Node<Object>::GetChildIndex(-1.0*rootOffset), root);
			root = rootIT;
		}
	}

	template<typename Object>
	void Octree<Object>::Update(objCollIT<Object> objIT)
	{
		//Find closest upper node that contains the object
		std::optional<nodesCollIT<Object>> newPlace = SearchUp(*objIT);

		nodesCollIT<Object> nodeIT;
		if (!newPlace)
		{
			ExpandTree(objIT->object);
			nodeIT = root;
		}
		else
			nodeIT = newPlace.value();

		//Push the object down the tree

		//Node has children or has too many objects
		//Add +1 if the object is currently in this node=does not count itself twice
		size_t limit = splitLimit + (objIT->GetParent() == nodeIT ? 1 : 0);
		while (nodeIT->HasChildren() || nodeIT->objects.size() >= limit)
		{
			auto optChildIT = PushDown(objIT->object, nodeIT);
			if (!optChildIT)break;
			nodeIT = *optChildIT;
		}
		auto objOldParent = objIT->GetParent();
		if (nodeIT != objOldParent)//Object must be moved
		{
			//Remove it from old nodeIT
			auto IT = std::find(objOldParent->objects.begin(), objOldParent->objects.end(), objIT);
			objOldParent->objects.erase(IT);
			//Add it to new nodeIT
			nodeIT->objects.push_back(objIT);
			objIT->SetParent(nodeIT);
		}
	}
	template<typename Object>
	void Octree<Object>::Validate()const
	{
		std::vector<bool> visitedNodes(nodes.Size(), false);
		int x;
		for (const auto& node : nodes)
		{
			for (int i = 0; i < 8; ++i)
			{
				if (node.HasChild(i))
				{
					auto child = node.GetChild(i);
					if (visitedNodes[child._index])
						x = 5;
					visitedNodes[child._index] = true;
				}
			}
			for (const auto& object : node.objects)
			{
				auto parent = object->GetParent();
				if (&*parent != &node)
					x = 5;
			}
		}
		for (const auto& object : objects)
		{
			auto& parent = *object.GetParent();
			bool found = false;
			for (const auto& objIT : parent.objects)
				if (&*objIT == &object)
					found = true;
			if (!found)
				return;
		}
	}
}


#endif
