#ifndef OCTREE_SOLVER_OCTREECOLLECTION_HEADER
#define OCTREE_SOLVER_OCTREECOLLECTION_HEADER


#include <iterator>
#include <vector>
#include <cassert>
#include "CommonTypes.hpp"

namespace collSim
{
	//TODO add required functions for randomAcessIterator
	template<typename T>
	struct OctreeCollectionIterator
	{
	public:
		constexpr const static bool isConst = std::is_const_v<T>;
		using value_type = T;
		using reference = T & ;
		using pointer = T * ;
		using difference_type = ptrdiff_t;
		using iterator_category = std::random_access_iterator_tag;
		using container = std::conditional_t<isConst, const std::vector<std::remove_cv_t<T>>, std::vector<T>>;

		OctreeCollectionIterator() :
			_container(nullptr), _index(0) {}
		OctreeCollectionIterator(container* cont, size_t index) :
			_container(cont), _index(index) {}
		OctreeCollectionIterator<T>& operator++()
		{
			++_index;
			return *this;
		}
		OctreeCollectionIterator<T> operator++(int)
		{
			auto&& tmp = OctreeCollectionIterator<T>(*this);
			++(*this);
			return std::move(tmp);
		}
		OctreeCollectionIterator<T>& operator--()
		{
			--_index;
			return *this;
		}
		OctreeCollectionIterator<T> operator--(int)
		{
			auto&& tmp = OctreeCollectionIterator<T>(*this);
			--(*this);
			return std::move(tmp);
		}
		pointer operator->()
		{
			return &((*_container)[_index]);
		}
		const pointer operator->() const
		{
			return &((*_container)[_index]);
		}
		reference operator*()
		{
			return ((*_container)[_index]);
		}
		const reference operator*() const
		{
			return((*_container)[_index]);
		}
		operator OctreeCollectionIterator<const T>() const
		{
			return OctreeCollectionIterator<const T>(_container, _index);
		}
		operator bool() const
		{
			return _container != nullptr;
		}
	public:
		container * _container;
		size_t _index;
	};
	template<typename T>
	bool operator==(OctreeCollectionIterator<T>l, OctreeCollectionIterator<T>r)
	{
		if (l._container && l._container == r._container && l._index == r._index)
			return true;
		//Default constructed ITs can be used as end
		else if (!l._container && !r._container)
			return true;
		else if (!l._container)
			return r._container->size() == r._index;
		else if (!r._container)
			return l._container->size() == l._index;
		else
			return false;
	}
	template<typename T>
	bool operator!=(OctreeCollectionIterator<T>l, OctreeCollectionIterator<T>r)
	{
		return !(l == r);
	}
	template<typename T>
	bool operator<(OctreeCollectionIterator<T>l, OctreeCollectionIterator<T>r)
	{
		return l._index < r._index;
	}

	//Wrapper around std::vector that does not invalidates all iterator on removal of an element.
	//-Removal is implement with swap with the last element whose iterators can be correct with optional callback.
	template<typename T>
	class OctreeCollection
	{
	private:
		using container = std::vector<T>;
	public:
		using value_type = typename container::value_type;
		using reference = typename container::reference;
		using const_reference = typename container::const_reference;
		using pointer = typename container::pointer;
		using const_pointer = typename container::const_pointer;
		using iterator = OctreeCollectionIterator<value_type>;
		using const_iterator = OctreeCollectionIterator<const value_type>;
		//reverse,creverse
		//diff, size
		template<typename... Args>
		iterator emplace_back(Args&&...args)
		{
			vec.emplace_back(std::forward<Args>(args)...);
			return iterator(&vec, vec.size() - 1);
		}
		size_t Size() const noexcept
		{
			return vec.size();
		}
		reference Back()
		{
			return vec.back();
		}
		const_reference Back() const
		{
			return vec.back();
		}
		iterator operator[](size_t index)
		{
			return iterator(&vec, index);
		}
		const_iterator operator[](size_t index) const
		{
			return const_iterator(&vec, index);
		}
		//Removes object at given position in O(1) by swapping it with the last object in the collection
		// - Invalidates iterators pointing to it. Relocator can fix that.
		//Relocator must have operator()(T& movedElement,iterator oldPosition, iterator newPosition)
		template<typename Relocator>
		void Remove(const iterator& at, Relocator&& relocator)
		{
			//using std::swap;
			assert(at._container == &vec);
			assert(at);
			if (at._index == vec.size() - 1)
				vec.pop_back();
			else
			{				
				
				swap(vec[at._index], vec[vec.size() - 1]);
				//Notify the moved object that it was indeed moved
				relocator(vec[at._index], iterator(&vec, vec.size()-1),iterator(&vec, at._index));
				vec.pop_back();
			}
		}

		iterator begin()
		{
			return iterator(&vec, 0);
		}
		const_iterator begin()const
		{
			return cbegin();
		}
		const_iterator cbegin()const
		{
			return const_iterator(&vec, 0);
		}
		iterator end()
		{
			return iterator(&vec, vec.size());
		}
		const_iterator end()const
		{
			return cend();
		}
		const_iterator cend()const
		{
			return const_iterator(&vec, vec.size());
		}
	private:
		std::vector<T> vec;
	};


}
#endif // !OCTREE_SOLVER_OCTREECOLLECTION_HEADER
