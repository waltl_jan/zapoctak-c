#include "OctreeSolver.hpp"

#include <random>
#include "OctreeSolver/Octree.hpp"
#include "Source/Geometry/Sphere.hpp"
namespace collSim
{

	namespace
	{
		//Returns point(with respect to AABB's center) on the AABB in direction of passed vector.
		//AKA Cube mapping
		Vec3d MapVecToCube(const AABB& aabb, const Vec3d& vec)
		{
			double max = std::max(std::abs(vec.x), std::max(std::abs(vec.y), std::abs(vec.z)));
			return vec / max * aabb.halfDist;
		}
		double CalculateMass(const Sphere& s)
		{
			return 4.0 / 3.0 * PI<double> * s.radius*s.radius*s.radius;
		}
	}
	bool Contains(const Sphere& s, const AABB& cube)
	{
		auto offset = cube.center - s.position;
		return std::max(std::abs(offset.x), std::max(std::abs(offset.y), std::abs(offset.z))) < cube.halfDist - s.radius;
	}
	bool Intersect(const Sphere& l, const Sphere& r)
	{
		return (l.position - r.position).LengthSq() < (l.radius + r.radius)*(l.radius + r.radius);
	}
	bool Intersect(const Sphere& r, const AABB& cube)
	{
		return r.radius*r.radius - MapVecToCube(cube, r.position - cube.center).LengthSq() > 0;
	}
	AABB GetAABB(const Sphere& s)
	{
		return AABB(s.position, s.radius);
	}

	void ResolveSpheres(Sphere& left, Sphere& right)
	{
		auto contactNormal = left.position - right.position;
		auto cNLength = contactNormal.Length();
		contactNormal /= cNLength;
		double intersectionDepth = left.radius + right.radius - cNLength;
		if (intersectionDepth > 0.0)
		{
			//Velocities along contact normal
			auto leftNVel = DotProduct(left.velocity, contactNormal)*contactNormal;
			auto rightNVel = DotProduct(right.velocity, contactNormal)*contactNormal;
			//Parallel velocities (parallel to collison plane)
			auto leftPVel = left.velocity - leftNVel;
			auto rightPvel = right.velocity - rightNVel;
			//Mass
			auto lMass = CalculateMass(left);
			auto rMass = CalculateMass(right);
			auto masses = lMass + rMass;

			//Push spheres apart
			left.position += intersectionDepth * contactNormal*rMass / masses;
			right.position -= intersectionDepth * contactNormal*lMass / masses;

			//Objects are not already moving apart or just resting
			if (DotProduct(leftNVel, rightNVel) <= 0 && !AlmostEqual(leftNVel.LengthSq(), 0.0) || !AlmostEqual(rightNVel.LengthSq(), 0.0))
			{

				//Set right one as ref frame
				//Relative velocity of left sphere
				auto rSpeed = rightNVel.Length();
				//Left and right velocities along the normal are opposite because of the negative dotproduct
				auto speed = leftNVel.Length() + rSpeed;
				//Elastic collision in 1D, where right sphere is stationary - solution given by 2 eqs:
				// rMass*speed = lMass*lNewSpeed + rMass*rNewSpeed
				// rMass*speed^2 = lMass*lNewSpeed^2 + rMass*rNewSpeed^2;
				auto lNewSpeed = speed - 2 * speed*rMass / masses;
				auto rNewSpeed = 2 * speed*lMass / masses;

				if (AlmostEqual(rSpeed, 0.0))
				{
					auto speedDir = leftNVel / speed;

					left.velocity = leftPVel + lNewSpeed * speedDir;
					right.velocity = rightPvel + rNewSpeed * speedDir;
				}
				else
				{
					auto speedDir = -1.0*rightNVel / rSpeed;
					//Parallel velocities remain unchanged
					//Velocities along the normal are converted back to original ref frame
					left.velocity = leftPVel + (lNewSpeed*speedDir + rightNVel);
					right.velocity = rightPvel + (rNewSpeed*speedDir + rightNVel);
				}
			}
		}
	}

	OctreeSolver::OctreeSolver(std::size_t numSpheres, std::size_t splitLimit, double minSize, double maxSize, bool collCube) :
		octree(AABB(Vec3d(0.0, 0.0, 0.0), 1.0), splitLimit), collCube(collCube)
	{
		//#define DEBUG_NEWTON_CRADLE_SCENE
#ifdef DEBUG_NEWTON_CRADLE_SCENE
		//Newton's cradle
		octree.Add(Sphere{ Vec3d(0.25,+0.8,-0.25),Vec3d(0.0,-0.5,0.0),0.05 });
		octree.Add(Sphere{ Vec3d(0.25,+0.2,-0.25),Vec3d(),0.05 });
		octree.Add(Sphere{ Vec3d(0.25,+0.3,-0.25),Vec3d(),0.05 });
		octree.Add(Sphere{ Vec3d(0.25,+0.1,-0.25),Vec3d(),0.05 });
		octree.Add(Sphere{ Vec3d(0.25,+0.0,-0.25),Vec3d(),0.05 });
		octree.Add(Sphere{ Vec3d(0.25,-0.1,-0.25),Vec3d(),0.05 });
		octree.Add(Sphere{ Vec3d(0.25,-0.2,-0.25),Vec3d(),0.05 });
		octree.Add(Sphere{ Vec3d(0.25,-0.3,-0.25),Vec3d(),0.05 });
#else//Uniform distribution of spheres
		std::mt19937 mt(std::random_device{}());
		//std::mt19937 mt(0);
		std::uniform_real_distribution<double> dist(0.0, 1.0);
		for (int i = 0; i < numSpheres; ++i)
		{
			Sphere sphere;
			sphere.position = Vec3d(dist(mt), dist(mt), dist(mt))*2.0 - Vec3d(1.0, 1.0, 1.0);//-1.0 to +1.0
			sphere.velocity = (Vec3d(dist(mt), dist(mt), dist(mt))*0.3 - Vec3d(0.15, 0.15, 0.15));//-0.3 to 0.3
			sphere.radius = minSize + dist(mt)*(maxSize-minSize);//0.01 - 0.1
			sphere.color = Vec3d(1.0, 1.0, 1.0);
			octree.Add(std::move(sphere));
		}
#endif
		octree.UpdateTree();
	}

	void OctreeSolver::Resolve(double deltaT)
	{
		OctreeStats stats;
		octree.UpdateTree(&stats);

		for (auto IT = octree.GetObjects().begin(); IT != octree.GetObjects().end(); ++IT)
		{
			if (collCube)
				CollWithCube(IT->object);
			auto collIT = octree.QuerySubTree(IT, &stats.collChecks);
			if (collIT)
				ResolveSpheres(IT->object, collIT.value()->object);
			IT->object.position += IT->object.velocity*deltaT;
		}
		if (statsCallback)
			statsCallback(stats);
	}
	void OctreeSolver::CollWithCube(Sphere& s)
	{
		if (s.velocity.x > 0 && s.position.x + s.radius > 1.0)
		{
			s.position.x = 2.0 - s.position.x - 2.0*s.radius;
			s.velocity.x *= -1.0;
		}
		if (s.velocity.y > 0 && s.position.y + s.radius > 1.0)
		{
			s.position.y = 2.0 - s.position.y - 2.0*s.radius;
			s.velocity.y *= -1.0;
		}
		if (s.velocity.z > 0 && s.position.z + s.radius > 1.0)
		{
			s.position.z = 2.0 - s.position.z - 2.0*s.radius;
			s.velocity.z *= -1.0;
		}
		if (s.velocity.x < 0 && s.position.x - s.radius < -1.0)
		{
			s.position.x = -2.0 - s.position.x + 2.0*s.radius;
			s.velocity.x *= -1.0;
		}
		if (s.velocity.y < 0 && s.position.y - s.radius < -1.0)
		{
			s.position.y = -2.0 - s.position.y + 2.0*s.radius;
			s.velocity.y *= -1.0;
		}
		if (s.velocity.z < 0 && s.position.z - s.radius < -1.0)
		{
			s.position.z = -2.0 - s.position.z + 2.0*s.radius;
			s.velocity.z *= -1.0;
		}
	}
}
