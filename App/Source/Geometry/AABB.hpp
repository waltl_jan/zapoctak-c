#ifndef GEOMETRY_AXIS_ALIGNED_BOUNDING_CUBE_HEADER
#define GEOMETRY_AXIS_ALIGNED_BOUNDING_CUBE_HEADER

#include "Source/Math/Math.hpp"
#include <algorithm>
namespace collSim
{
	class AABB
	{
	public:
		AABB(const Vec3d& center, double halfDist) : center(center), halfDist(halfDist) {}
		Vec3d center;
		double halfDist;

		friend void swap(AABB& left, AABB& right);
	};
	inline void swap(AABB& left, AABB&right)
	{
		using std::swap;
		swap(left.center, right.center);
		swap(left.halfDist, right.halfDist);
	}
}

#endif