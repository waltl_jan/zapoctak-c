#ifndef GEOMETRY_SPHERE_34072147_HEADER
#define GEOMETRY_SPHERE_34072147_HEADER


#include "Source/Math/Math.hpp"
#include <vector>

namespace collSim
{
	struct Sphere
	{
		//Just to test that Octree can properly handle move-only objects
		Sphere() = default;
		Sphere(const Sphere&) = delete;
		Sphere(Sphere&&) = default;
		Sphere& operator=(const Sphere&) = delete;
		Sphere& operator=(Sphere&&) = default;
		Vec3d position;
		Vec3d velocity;
		double radius;
		Vec3d color;
	};
	using spheres_t = std::vector < Sphere>;
}

#endif