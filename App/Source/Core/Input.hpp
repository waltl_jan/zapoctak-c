#ifndef CORE_INPUT_HEADER
#define CORE_INPUT_HEADER

#include <array>
#include <algorithm>
#include "Source/Math/Math.hpp"
#include "Key.hpp"
namespace collSim
{
	//Currently Input can be set only by GraphicsManager
	class GraphicsManager;
	//Manages Input from the User
	class Input
	{
	public:
		enum class state
		{
			press,
			hold,
			release
		};
		enum class mouse
		{
			left, right
		};
		Input()
		{
			std::for_each(keys.begin(), keys.end(), [](state& s) {s = state::release; });
			mouseButton.left = mouseButton.right = state::release;
		}
		bool IsKeyPressed(int key) const
		{
			return keys[key] != state::release;
		}
		bool IsMousePressed(mouse button)const
		{
			return button == mouse::left ? mouseButton.left != state::release : mouseButton.right != state::release;
		}
		const Vec2d& GetMousePos() const
		{
			return mousePos;
		}
		const Vec2d& GetMouseScrollDelta() const
		{
			return mouseScrollDelta;
		}

		void SetKey(Key<GraphicsManager>, int key, state state)
		{
			keys[key] = state;
		}
		void SetMouseButton(Key<GraphicsManager>, mouse button, state state)
		{
			button == mouse::left ? mouseButton.left = state : mouseButton.right = state;
		}
		void SetMousePos(Key<GraphicsManager>, const Vec2d mousePosition)
		{
			mousePos = mousePosition;
		}
		void SetMouseScroll(Key<GraphicsManager>, const Vec2d mouseScrollDelta)
		{
			this->mouseScrollDelta = mouseScrollDelta;
		}
	private:
		std::array<state, 512> keys;
		struct
		{
			state left, right;
		}mouseButton;
		//In [-1,1],x right, y up
		Vec2d mousePos;
		Vec2d mouseScrollDelta;
	};
}

#endif
