#include "Simulation.hpp"

#include <chrono>
#include <iostream>
#include "Camera.hpp"

namespace collSim
{
	Simulation::Simulation()
	{
		ResetTimers();

	}
	void Simulation::Start(CollisionSolver& collSolver, visuals::Renderer& renderer, const clock_t::duration& deltaT)
	{
		ResetTimers();
		running = true;
		while (running)
		{
			TickTimers();
			while (accumulator > deltaT)
			{
				double ms = double(deltaT.count()) / clock_t::duration::period::den  * clock_t::duration::period::num;

				Update(collSolver, ms);

				accumulator -= deltaT;
			}
			if (!renderer.Render())
				running = false;
		}
	}
	void Simulation::ResetTimers()
	{
		prevTime = start = clock_t::now();
		frameTime = accumulator = accumulator.zero();
	}
	void Simulation::TickTimers()
	{
		using namespace std::chrono_literals;

		auto now = clock_t::now();
		frameTime = now - prevTime;
		prevTime = now;
		accumulator += frameTime < 200ms ? frameTime : 200ms;
	}
	void Simulation::Update(CollisionSolver& collSolver, double deltaT)
	{
		collSolver.Resolve(deltaT);
	}
}