#include "Camera.hpp"
#include <cstdint>
#include "Input.hpp"
#include <GLFW/glfw3.h>
namespace collSim
{
	namespace
	{
		constexpr size_t matSize = 16 * sizeof(float);
		constexpr size_t bufferSize = 3 * matSize;//TODO change if inverses are added
		//Cameras' UBOs will use this binding point
		constexpr GLuint UBOBinding = 4;
	}

	Camera::Camera(float FOV, float AR, float near, float far)
	{
		proj = MakePerspective(FOV, AR, near, far);
		view = MakeIdentity<float>();
		glGenBuffers(1, &UBO);
		glBindBuffer(GL_UNIFORM_BUFFER, UBO);
		glBufferData(GL_UNIFORM_BUFFER, (GLsizeiptr)bufferSize, nullptr, GL_STREAM_DRAW);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

	void Camera::Bind(Key<GraphicsManager>)
	{
		glBindBufferBase(GL_UNIFORM_BUFFER, UBOBinding, UBO);
	}
	void Camera::UnBind(Key<GraphicsManager>)
	{
		glBindBufferBase(GL_UNIFORM_BUFFER, UBOBinding, 0);
	}
	void Camera::ResizeProj(Key<GraphicsManager>, float newAR)
	{
		proj[5] = proj[0] * newAR;
		SubmitMatrices();
	}
	Camera::~Camera()
	{
		if (UBO != 0)
			glDeleteBuffers(1, &UBO);
	}
	void Camera::LookAt(const Vec3d& newCamPos, const Vec3d& newTargetPos, const Vec3d& newUpDir/* = Vec3d(0.0, 1.0, 0.0)*/)
	{
		camPos = newCamPos;
		targetPos = newTargetPos;
		auto dir = (camPos - targetPos).Normalize();

		rightDir = CrossProduct(newUpDir, dir).Normalize();
		upDir = CrossProduct(dir, rightDir);
		view[0] = float(rightDir.x); view[4] = float(rightDir.y);	view[8] = float(rightDir.z);	view[12] = -float(DotProduct(camPos, rightDir));
		view[1] = float(upDir.x);	 view[5] = float(upDir.y);		view[9] = float(upDir.z);		view[13] = -float(DotProduct(camPos, upDir));
		view[2] = float(dir.x);		 view[6] = float(dir.y);		view[10] = float(dir.z);		view[14] = -float(DotProduct(camPos, dir));
		view[3] = 0;				 view[7] = 0;					view[11] = 0;					view[15] = 1;

		SubmitMatrices();
	}
	void Camera::Subscribe(gl::Shader& shader, const std::string& blockName /*= "CameraMatrices"*/) const
	{
		shader.SetUniformBlockBinding(blockName, UBOBinding);
	}

	Vec3d Camera::CamPos()const
	{
		return camPos;
	}
	Vec3d Camera::TargetPos()const
	{
		return targetPos;
	}
	Vec3d Camera::RightDir()const
	{
		return rightDir;
	}
	Vec3d Camera::UpDir()const
	{
		return upDir;
	}
	void Camera::SubmitMatrices()
	{
		//This is layout of buffer:
		//
		//mat4 projection;
		//mat4 view;
		//mat4 projView;
		//mat4 invProj;
		//mat4 invView;
		//mat4 invProjView;
		//
		GLint currentBuffer;
		glGetIntegeri_v(GL_UNIFORM_BUFFER_BINDING, UBOBinding, &currentBuffer);
		glBindBufferBase(GL_UNIFORM_BUFFER, UBOBinding, UBO);
		glBufferSubData(GL_UNIFORM_BUFFER, 0 * matSize, matSize, proj.Data());
		glBufferSubData(GL_UNIFORM_BUFFER, 1 * matSize, matSize, view.Data());
		glBufferSubData(GL_UNIFORM_BUFFER, 2 * matSize, matSize, (proj*view).Data());
		//TODO inverses if needed
		//glBufferSubData(GL_UNIFORM_BUFFER, 3 * matSize, matSize, Inverse(projection).Data());
		//glBufferSubData(GL_UNIFORM_BUFFER, 4 * matSize, matSize, Inverse(view).Data());
		//glBufferSubData(GL_UNIFORM_BUFFER, 5 * matSize, matSize, Inverse(projection*view).Data());
		glBindBufferBase(GL_UNIFORM_BUFFER, UBOBinding, static_cast<GLuint>(currentBuffer));

	}
	Mat4f Camera::InvView() const
	{
		//Top 3x3 of view matrix is ortogonal
		Mat4f invView = Transpose(view);
		//Inverse of translation is -1*translation;
		invView[3] *= -1;
		invView[7] *= -1;
		invView[11] *= -1;
		return invView;
	}
	void CameraControls::Update(Input* input)
	{
		bool isRotating = input->IsMousePressed(Input::mouse::left);
		if (isRotating)
		{
			if (!wasRotating)//First frame of rotation -> Cache camera
				CacheCamera(input->GetMousePos());
			wasRotating = true;
		}
		if (isRotating)
		{
			Vec2d endPoint = input->GetMousePos();
			Mat4d rotMatrix = TrackBallRotation(cache.startPoint, endPoint, cache.invView, true);
			//Rotate point around camera's target
			auto delta4 = rotMatrix * (Vec4d(cache.camPos - cache.targetPos, 1.0));
			//Compute new up vector to enable camera roll
			auto up4 = rotMatrix * Vec4d(cache.camUp, 0.0);

			cam->LookAt(cache.targetPos + Vec3d(delta4.x, delta4.y, delta4.z), cache.targetPos, Vec3d(up4.x, up4.y, up4.z));
		}
		else
		{
			wasRotating = false;
			Vec3d dir = (cam->CamPos() - cam->TargetPos()).Normalize();
			double y = input->GetMouseScrollDelta().y;
			y = std::clamp(y, -1.0, 1.0);
			if (y != 0.0)
				cam->LookAt(cam->CamPos() + input->GetMouseScrollDelta().y*dir, cam->TargetPos(), cam->UpDir());
		}

	}
	Mat4d CameraControls::TrackBallRotation(Vec2d startPoint, Vec2d endPoint, const Mat4f & viewToWorld, bool onFront)
	{
		double sign = onFront ? 1.0f : -1.0f;
		//Screen->Sphere
		double startLength = startPoint.LengthSq();
		Vec3d startVector(startPoint.x, startPoint.y, startLength >= 1.0 ? 0.0 : sign * sqrt(1.0 - startLength));
		double endLength = endPoint.LengthSq();
		Vec3d endVector(endPoint.x, endPoint.y, endLength >= 1.0 ? 0.0 : sign * sqrt(1.0 - endLength));
		//If length > 1.0
		startVector.Normalize();
		endVector.Normalize();
		//angle is immune to coord transformations
		double angle = acos(DotProduct(startVector, endVector));
		if (angle > 0.01)//So the axis is well defined
		{
			auto axis = CrossProduct(endVector, startVector).Normalize();
			auto axis4 = viewToWorld * Vec4d(axis, 0.0);//So returned matrix is in world coordinates
			return MakeRotation(Vec3d(axis4.x, axis4.y, axis4.z), angle);
		}
		else//Do not rotate
			return MakeIdentity<double>();
	}
	void CameraControls::CacheCamera(const Vec2d& screenStartPos)
	{
		cache.camPos = cam->CamPos();
		cache.targetPos = cam->TargetPos();
		cache.camUp = cam->UpDir();
		cache.invView = cam->InvView();
		cache.startPoint = screenStartPos;
	}

}