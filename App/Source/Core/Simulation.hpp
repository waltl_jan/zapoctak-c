#ifndef CORE_SIMULATION_HEADER
#define CORE_SIMULATION_HEADER

#include <cstdint>
#include <vector>
#include <memory>
#include <chrono>
#include "Source/CollisionSolvers/CollisionSolver.hpp"
#include "Source/Geometry/Sphere.hpp"
#include "Source/Visuals/Renderer.hpp"
namespace collSim
{
	//Main class of application that has while(true) loop.
	class Simulation
	{
	public:
		using clock_t = std::chrono::steady_clock;

		Simulation();
		Simulation(const Simulation&) = delete;
		Simulation(Simulation&&) = delete;
		Simulation& operator=(const Simulation&) = default;
		Simulation& operator=(Simulation&&) = default;
		~Simulation() = default;

		void Start(CollisionSolver& collSolver,visuals::Renderer& renderer,
			const clock_t::duration& deltaT = std::chrono::milliseconds(10));

	private:
		void ResetTimers();
		void TickTimers();
		//deltaT in seconds
		void Update(CollisionSolver& collSolver,double deltaT);

		clock_t::duration frameTime, accumulator;
		clock_t::time_point prevTime, start;

		bool running;
	};
}


#endif