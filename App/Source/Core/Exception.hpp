#ifndef COLLSIM_CORE_EXCEPTION_HEADER
#define COLLSIM_CORE_EXCEPTION_HEADER

#include <stdexcept>

namespace collSim
{
	class Exception :public std::runtime_error
	{
		using std::runtime_error::runtime_error;
	};
}

#endif