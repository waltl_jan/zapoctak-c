#ifndef CORE_KEY_32572104_HEADER
#define CORE_KEY_32572104_HEADER


namespace collSim
{
	//Emty class that can be constructed only by given owner
	template<typename Owner>
	class Key final
	{
	private:
		friend Owner;
		Key() = default;
		Key(const Key&) = default;
		Key(Key&&) = default;
		Key& operator=(const Key&) = default;
		Key& operator=(Key&&) = default;

	};
}

#endif