#ifndef IMGUI_BACKEND_HEADER
#define IMGUI_BACKEND_HEADER

#include <memory>

struct GLFWwindow;
struct ImDrawData;

namespace collSim
{
	namespace gl
	{
		class Shader;
	}
	//Integrates ImGui library into program
	//Handles its initialization and also sets callbacks for mouse&keyboard
	class IMGuiBackend
	{
	public:
		//Needs valid GLFWindow to register callbacks
		IMGuiBackend(GLFWwindow* win);
		~IMGuiBackend();

		void NewFrame(double lastFrameTime);
		void Render();
	private:
		void SetImGUISettings();
		void SetStyle();
		void LoadShader();
		void CreateBuffers();
		void LoadFontTexture();
		void static RenderFnc(ImDrawData* draw_data);

		GLFWwindow* win;
		//Shader to use for rendering of ImGui
		static std::unique_ptr<gl::Shader> shader;
		//Font texture and buffer for vertex data
		static unsigned int textID, VAO, IBO, VBO;
	};
}


#endif