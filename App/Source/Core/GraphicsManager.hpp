#ifndef COLLSIM_CORE_GRAPHICS_MANAGER_HEADER
#define COLLSIM_CORE_GRAPHICS_MANAGER_HEADER

#include <string>
#include <memory>
#include "Source/Core/Camera.hpp"
#include "ImGuiBackend.hpp"
struct GLFWwindow;

namespace collSim
{
	class Camera;
	class Input;
	class CameraControls;
	//Class that initializes GLFW library, constructs main window and creates its OpenGL context.
	//Currently only one should be alive at the time.
	class GraphicsManager
	{
	public:
		//Creates window with given parameters.
		//Input is used to redirect user input, must outlive this instance.
		GraphicsManager(const std::string& title, int w, int h, Input* input);
		GraphicsManager(const GraphicsManager&) = delete;
		GraphicsManager(GraphicsManager&&) = default;
		GraphicsManager& operator=(const GraphicsManager&) = delete;
		GraphicsManager& operator=(GraphicsManager&&) = default;
		~GraphicsManager();
		//Returns false if window should be closed
		//Clears screen and binds the camera
		bool NewFrame();
		void Render();
		Camera* GetCamera();
	private:
		void InitGLFW(const std::string& title, int w, int h);
		void InitOpenGL(int w, int h);
		static void ErrorCallback(int err, const char * description);
		static void ResizeCallback(GLFWwindow* win, int width, int height);
		static void MousePosCallback(GLFWwindow* win, double xPos, double yPos);
		static void MouseClickCallback(GLFWwindow* win, int button, int action, int mods);
		static void KeyCallback(GLFWwindow* win, int key, int, int action, int mods);
		void static MouseScrollCallback(GLFWwindow* win, double dx, double dy);

		static bool errTrigger;
		static std::string error;
		GLFWwindow* win;
		Input* input;
		int width, height;
		std::unique_ptr<Camera> cam;
		std::unique_ptr<CameraControls> camControls;
		std::unique_ptr<IMGuiBackend> imgui;
	};
}

#endif