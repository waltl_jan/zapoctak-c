#include "GraphicsManager.hpp"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <cassert>
#include <imgui/imgui.h>

#include "Exception.hpp"
#include "Camera.hpp"
#include "Input.hpp"

namespace collSim
{
	std::string GraphicsManager::error;
	bool GraphicsManager::errTrigger = false;

	GraphicsManager::GraphicsManager(const std::string& title, int w, int h, Input* input)
		:input(input), width(w), height(h)
	{
		assert(w > 0 && h > 0);
		InitGLFW(title, w, h);
		InitOpenGL(w, h);

		cam = std::make_unique<Camera>(45.0f, 1.0f, 0.01f, 100.0f);
		cam->LookAt(Vec3d(5.0, 0.0, 5.0), Vec3d(0.0, 0.0, 0.0));
		camControls = std::make_unique<CameraControls>(cam.get());
		imgui = std::make_unique<IMGuiBackend>(win);
	}
	bool GraphicsManager::NewFrame()
	{
		if (glfwWindowShouldClose(win))
			return false;
		else
		{
			cam->Bind(Key<GraphicsManager>{});
			camControls->Update(input);
			input->SetMouseScroll(Key<GraphicsManager>{}, Vec2d{ 0.0,0.0 });
			glfwPollEvents();
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			//TODO set correct frame time
			imgui->NewFrame(0.16);
			return true;
		}
	}
	void GraphicsManager::Render()
	{
		imgui->Render();
		glfwSwapBuffers(win);
	}
	GraphicsManager::~GraphicsManager()
	{
		glfwTerminate();
	}

	Camera* GraphicsManager::GetCamera()
	{
		return cam.get();
	}
	void GraphicsManager::InitGLFW(const std::string& title, int w, int h)
	{
		if (glfwInit() == GL_FALSE)
			throw Exception("GLFW initialization failed.");
		glfwSetErrorCallback(ErrorCallback);

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
		glfwWindowHint(GLFW_SAMPLES, 1);//Anti-aliasing
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		win = glfwCreateWindow(w, h, title.c_str(), nullptr, nullptr); // create actual window
		if (win == nullptr) // if that fails
			throw Exception("Unable to create Window, reason: " + error);

		glfwSetWindowUserPointer(win, this);
		glfwSetWindowSizeCallback(win, GraphicsManager::ResizeCallback);
		glfwSetKeyCallback(win, GraphicsManager::KeyCallback);
		glfwSetMouseButtonCallback(win, GraphicsManager::MouseClickCallback);
		glfwSetCursorPosCallback(win, GraphicsManager::MousePosCallback);
		glfwSetScrollCallback(win, GraphicsManager::MouseScrollCallback);
		//glfwSetWindowPos(win, 10, 20);
		glfwMakeContextCurrent(win);
		glfwSwapInterval(1);
	}
	void GraphicsManager::InitOpenGL(int w, int h)
	{
		if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) // tries to initialize glad
		{
			glfwTerminate();
			throw Exception("GLAD initialization failed.");
		}

		glViewport(0, 0, w, h);
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	}

	void GraphicsManager::ErrorCallback(int err, const char * description)
	{
		error = "GLFW error: " + std::to_string(err) + ": " + description;
		if (err == 65543)//Failed to create context - version might be too low
			error += "\n Make sure your version of OpenGL is atleast 3.3";
		errTrigger = true;
	}

	void GraphicsManager::ResizeCallback(GLFWwindow* win, int width, int height)
	{
		GraphicsManager* man = reinterpret_cast<GraphicsManager*>(glfwGetWindowUserPointer(win));
		man->cam->ResizeProj(Key<GraphicsManager>{}, float(width) / height);
		man->width = width;
		man->height = height;
		glViewport(0, 0, width, height);
		ImGuiIO& io = ImGui::GetIO();
		io.DisplaySize = { static_cast<float>(width),static_cast<float>(height) };

	}
	void GraphicsManager::KeyCallback(GLFWwindow* win, int key, int, int action, int)
	{
		GraphicsManager* man = reinterpret_cast<GraphicsManager*>(glfwGetWindowUserPointer(win));

		Input::state state;
		if (action == GLFW_PRESS)
			state = Input::state::press;
		else if (action == GLFW_REPEAT)
			state = Input::state::hold;
		else
			state = Input::state::release;
		man->input->SetKey(Key<GraphicsManager>(), key, state);
		ImGuiIO& io = ImGui::GetIO();

		if (action == GLFW_PRESS)
			io.KeysDown[key] = true;
		if (action == GLFW_RELEASE)
			io.KeysDown[key] = false;

		io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
		io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
		io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
	}
	void GraphicsManager::MouseScrollCallback(GLFWwindow * win, double dx, double dy)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.MouseWheel = dy;
		GraphicsManager* man = reinterpret_cast<GraphicsManager*>(glfwGetWindowUserPointer(win));
		man->input->SetMouseScroll(Key<GraphicsManager>(), Vec2d(dx, dy));

	}
	void GraphicsManager::MousePosCallback(GLFWwindow* win, double xPos, double yPos)
	{
		GraphicsManager* man = reinterpret_cast<GraphicsManager*>(glfwGetWindowUserPointer(win));
		Vec2d mousePos = 2.0*Vec2d(xPos, yPos) / Vec2d(man->width, man->height) - Vec2d(1.0, 1.0);
		mousePos.y *= -1;//Y is up
		man->input->SetMousePos(Key<GraphicsManager>(), mousePos);
	}
	void GraphicsManager::MouseClickCallback(GLFWwindow* win, int button, int action, int mods)
	{
		GraphicsManager* man = reinterpret_cast<GraphicsManager*>(glfwGetWindowUserPointer(win));
		Input::state state;
		if (action == GLFW_PRESS)
			state = Input::state::press;
		else if (action == GLFW_REPEAT)
			state = Input::state::hold;
		else
			state = Input::state::release;
		Input::mouse mouseButton = button == GLFW_MOUSE_BUTTON_LEFT ? Input::mouse::left : Input::mouse::right;
		man->input->SetMouseButton(Key<GraphicsManager>(), mouseButton, state);
		ImGuiIO& io = ImGui::GetIO();

		if (0 <= button && button < 5)
		{
			if (action != GLFW_RELEASE)
				io.MouseDown[button] = true;
			else
				io.MouseDown[button] = false;

		}
	}



}