#include "ImGuiBackend.hpp"

#include <External/glad/glad.h>
#include <GLFW/glfw3.h>

#include <External/imgui/imgui.h>
#include <limits>
#include "Source/GL/Shader.hpp"
#include "Source/GL/GLError.hpp"
namespace collSim
{
	unsigned int IMGuiBackend::textID, IMGuiBackend::VAO, IMGuiBackend::IBO, IMGuiBackend::VBO;
	std::unique_ptr<gl::Shader> IMGuiBackend::shader;

	// Following code is based on documentation in imgui.h; imgui.cpp and examples 
	// on library's official page: https://github.com/ocornut/imgui


	IMGuiBackend::IMGuiBackend(GLFWwindow * win)
	{
		assert(win);//If nullptr, that means this is initializing first, which should not.
					// Or OpenGLBackend failed, which should've thrown - so does not end up here either.

		this->win = win;
		SetImGUISettings();
		LoadShader();
		CreateBuffers();
		LoadFontTexture();

		auto err = gl::CheckForError();
		if (err != gl::errors::noError)
			throw Exception("Failed to initialized IMGUI, because of following GL error: " + gl::TranslateError(err));
	}
	void IMGuiBackend::NewFrame(double lastFrameTime)
	{
		//Fill mouse position, because according to documentation needs to be done every frame,
		//Other things seem to work fine just using callbacks.

		ImGuiIO& io = ImGui::GetIO();
		double x, y;
		glfwGetCursorPos(win, &x, &y);
		io.MousePos = { static_cast<float>(x),static_cast<float>(y) };
		io.DeltaTime = static_cast<float>(lastFrameTime);
		shader->SetUniform4Mat("orthoProjMat", MakeOrtho(0.0f, io.DisplaySize.y, 0.0f, io.DisplaySize.x, -1.0f, 1.0f));

		ImGui::NewFrame();
	}
	void IMGuiBackend::Render()
	{
		ImGui::Render();
		this->RenderFnc(ImGui::GetDrawData());
	}
	IMGuiBackend::~IMGuiBackend()
	{
		glDeleteTextures(1, &textID);
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &IBO);
		glDeleteVertexArrays(1, &VAO);
		shader.reset(nullptr);
		ImGui::DestroyContext();
	}

	void IMGuiBackend::SetImGUISettings()
	{
		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO();


		//Map to translate GLFW keys to ImGui keys
		io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;
		io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
		io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
		io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
		io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
		io.KeyMap[ImGuiKey_PageUp] = GLFW_KEY_PAGE_UP;
		io.KeyMap[ImGuiKey_PageDown] = GLFW_KEY_PAGE_DOWN;
		io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
		io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
		io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
		io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
		io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
		io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
		io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
		io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
		io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
		io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
		io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
		io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

		int w, h;
		glfwGetWindowSize(win, &w, &h);
		io.DisplaySize = { static_cast<float>(w),static_cast<float>(h) };

		SetStyle();
	}

	void IMGuiBackend::SetStyle()
	{
		ImGui::StyleColorsClassic();
		ImGuiIO& io = ImGui::GetIO();

		//io.FontGlobalScale = 2.0f;
	}

	void IMGuiBackend::LoadShader()
	{
		const std::string vertSource = R"(
			#version 330 core

			layout(location=0)in vec2 pos;
			layout(location=1)in vec4 color;
			layout(location=2)in vec2 uv;
			
			//Pass them to fragment shader
			out vec4 fragCol;
			out vec2 fragUV;

			uniform mat4 orthoProjMat;
			void main()
			{
				fragCol=color;
				fragUV=uv;
				gl_Position = orthoProjMat*vec4(pos.xy,0.0,1.0);
			})";

		const std::string fragSource = R"(
			#version 330 core

			in vec2 fragUV;
			in vec4 fragCol;

			out vec4 outCol;

			uniform sampler2D text;

			void main()
			{
				outCol = fragCol * texture(text,fragUV.st);
			})";

		shader = std::make_unique<gl::Shader>(vertSource, fragSource);

		ImGuiIO& io = ImGui::GetIO();
		shader->SetUniform1i("text", 0);
	}

	void IMGuiBackend::CreateBuffers()
	{
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &IBO);

		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)offsetof(ImDrawVert, pos));
		glEnableVertexAttribArray(0);//Position
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)offsetof(ImDrawVert, col));
		glEnableVertexAttribArray(1);//Color
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)offsetof(ImDrawVert, uv));
		glEnableVertexAttribArray(2);//UV

		glBindVertexArray(0);

	}

	void IMGuiBackend::LoadFontTexture()
	{
		// Build texture atlas
		ImGuiIO& io = ImGui::GetIO();

		//Export font to texture
		unsigned char* pixels;
		int width, height;
		io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);
		//Create OpenGL texture
		glGenTextures(1, &textID);
		glBindTexture(GL_TEXTURE_2D, textID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		glBindTexture(GL_TEXTURE_2D, 0);

		io.Fonts->TexID = (void *)(intptr_t)textID;
		io.Fonts->ClearTexData();
	}
	void IMGuiBackend::RenderFnc(ImDrawData * draw_data)
	{
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_SCISSOR_TEST);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		ImGuiIO& io = ImGui::GetIO();
		shader->Bind();
		glBindVertexArray(VAO);

		for (int n = 0; n < draw_data->CmdListsCount; n++)
		{
			const ImDrawList* cmd_list = draw_data->CmdLists[n];
			const ImDrawIdx* idx_buffer_offset = 0;

			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert), (const GLvoid*)cmd_list->VtxBuffer.Data, GL_STREAM_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx), (const GLvoid*)cmd_list->IdxBuffer.Data, GL_STREAM_DRAW);

			for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
			{
				const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
				if (pcmd->UserCallback)
				{
					pcmd->UserCallback(cmd_list, pcmd);
				}
				else
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
					glScissor((int)pcmd->ClipRect.x, (int)(io.DisplaySize.y - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
					glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
				}
				idx_buffer_offset += pcmd->ElemCount;
			}
		}
		shader->UnBind();
		glBindVertexArray(0);
		glDisable(GL_SCISSOR_TEST);// We dont use it in rest of our program.
		glEnable(GL_DEPTH_TEST);
		auto err = gl::CheckErrorDBG();
		if (err != gl::errors::noError)
			throw Exception("IMGUI::Render failed, because of following GL error: " + gl::TranslateError(err));
	}
}
