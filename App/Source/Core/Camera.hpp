#ifndef CORE_CAMERA_HEADER
#define CORE_CAMERA_HEADER


#include "Source/Math/Math.hpp"
#include "Source/GL/Shader.hpp"
#include <glad/glad.h>
#include "Key.hpp"

namespace collSim
{
	class GraphicsManager;
	class Input;

	//Perspective camera
	class Camera
	{
	public:
		//AR=Aspects ratio of the screen (width/height)
		Camera(float FOV, float AR, float near, float far);
		Camera(const Camera&) = delete;
		Camera(Camera&&) = default;
		Camera& operator=(const Camera&) = delete;
		Camera& operator=(Camera&&) = default;
		~Camera();
		//Sets camera's view matrix in such way that camera is located at given positon and looks at desired target location.
		void LookAt(const Vec3d& newCamPos, const Vec3d& newTargetPos, const Vec3d& newUpDir = Vec3d(0.0, 1.0, 0.0));
		//If shader wants to draw from a camera's point of view, then it must contain following interface block:
		/*		std140 uniform **blockName**
		{
		mat4 projection;
		mat4 view;
		mat4 projView;
		} **instanceName**;*/
		void Subscribe(gl::Shader& shader, const std::string& blockName = "CameraMatrices") const;

		//Binds this camera for next drawing.( Updates its UBO with new matrices and binds UBO to binding point associated with all shaders)
		void Bind(Key<GraphicsManager>);
		void UnBind(Key<GraphicsManager>);
		//Adjusts projection matrix for when screen has been resized - sets new aspect ratio
		void ResizeProj(Key<GraphicsManager>, float newAR);

		Vec3d CamPos()const;
		Vec3d TargetPos()const;
		Vec3d RightDir()const;
		Vec3d UpDir()const;
		//Returns inverse of view matrix
		Mat4f InvView()const;
	private:

		void SubmitMatrices();

		GLuint UBO;
		Mat4f proj, view;
		Vec3d camPos, targetPos;
		Vec3d rightDir, upDir;
	};

	//Controls the camera via Trackball
	class CameraControls
	{
	public:
		CameraControls(Camera* cam) :
			wasRotating(false), cam(cam) {}
		//Updates camera based on current inputs
		void Update(Input* input);
	private:
		//mouseStartPoint is in [-1,1],x right, y up
		void CacheCamera(const Vec2d& mouseStartPoint);
		//Computes rotation matrix from movement between two point
		//by projecting those points onto a unit sphere and return a rotation matrix to rotate the sphere
		//so the start point ends up at the end point.
		//onFront - onto which side of the sphere project the points, effectively reverses the rotation
		Mat4d TrackBallRotation(Vec2d startPoint, Vec2d endPoint, const Mat4f & viewToWorld, bool onFront);
		struct
		{
			Vec3d camPos, camUp, targetPos;
			Mat4f invView;
			//In [-1,1],x right, y up
			Vec2d startPoint;
		}cache;
		bool wasRotating;
		Camera* cam;
	};
}

#endif