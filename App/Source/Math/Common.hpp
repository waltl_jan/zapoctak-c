#ifndef MATH_COMMON_3525209479124107_HEADER
#define MATH_COMMON_3525209479124107_HEADER

#include <cmath>
#include <cassert>
#include <cstdint>
#include "Source/Core/Exception.hpp"

namespace collSim
{
	constexpr uint32_t epsilonDist = 10;
	template<typename T> constexpr T epsilon = T(1e-10);

	template<typename T> constexpr T PI = T(3.1415926535);
	template<typename T> constexpr T G = T(6.67408e-11);

	inline bool AlmostEqual(float x, float y, float relEps = epsilon<float>)
	{
		//TODO ADD ULPs? https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
		auto absX = std::abs(x);
		auto absY = std::abs(y);
		if (absX > absY)
			return std::abs(x - y) <= absX * epsilon<float>;
		else
			return std::abs(x - y) <= absY * epsilon<float>;
	}
	inline bool AlmostEqual(double x, double y, double relEps = epsilon<double>)
	{
		if (x == 0.0 || y == 0.0)
			return std::abs(x - y) <= epsilon<double>;
		//TODO ADD ULPs? https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
		auto absX = std::abs(x);
		auto absY = std::abs(y);
		if (absX > absY)
			return std::abs(x - y) <= absX * epsilon<double>;
		else
			return std::abs(x - y) <= absY * epsilon<double>;
	}
	template<typename T>
	T DegToRad(T degs)
	{
		return degs*PI<T> / T(180);
	}
	template<typename T>
	T RadToDeg(T rad)
	{
		return rad* T(180) / PI<T>;
	}
}


#endif
