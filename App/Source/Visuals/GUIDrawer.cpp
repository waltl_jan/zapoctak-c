#include "GUIDrawer.hpp"
#include <imgui/imgui.h>
#include "Source/CollisionSolvers/OctreeSolver/Octree.hpp"


collSim::visuals::GUIDrawer::GUIDrawer()
{
	updCooldown = 0.0;
	lastCollectTime = std::chrono::high_resolution_clock::now();
}

void collSim::visuals::GUIDrawer::Draw()
{
	ImGui::SetNextWindowPos(ImVec2(5, 5), ImGuiCond_Once);
	ImGui::SetNextWindowBgAlpha(0.3f);
	ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiCond_Once);
	int flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove;
	if (ImGui::Begin("Example: Fixed Overlay", nullptr, flags))
	{
		ImGui::Text("Stats:");
		ImGui::Separator();
		ImGui::Text("Objects: %u", static_cast<unsigned int>(stats.objects));
		auto n2Checks = stats.objects*stats.objects;
		ImGui::Text("N^2 coll. checks: %u", static_cast<unsigned int>(n2Checks));
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Number of collision checks that n^2 solver would make.");
		ImGui::Text("Octree coll. checks: %u", static_cast<unsigned int>(stats.collChecks));
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Number of collision checks that octree solver made.\n That includes tests with tree's nodes and other objects.");
		ImGui::Text("%% saved: %.2f", (n2Checks - stats.collChecks) / (double)n2Checks *100.0);
		ImGui::Separator();
		ImGui::Text("OctreeStats:");
		ImGui::Separator();
		ImGui::Text("Nodes: %u", static_cast<unsigned int>(stats.nodes));
		ImGui::Text("Empty nodes: %u (%.2f %%)", static_cast<unsigned int>(stats.emptyNodes), stats.emptyNodes * 100.0f / stats.nodes);
		ImGui::Text("Avg depth: %.2f", stats.avgDepth);
		ImGui::Text("Max depth: %.2f", stats.maxDepth);
		ImGui::Text("Avg objects per node: %.2f", stats.avgObjectsPerNode);
		ImGui::Text("Max objects in a node: %u", static_cast<unsigned int>(stats.maxObjectsInNode));
		ImGui::Text("UpdateTree time: %.4fms", stats.updateTime.count() / static_cast<double>(decltype(stats.updateTime)::period::den) * 1000);
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("UpdateTree replaces moved objects into the tree. Splits full nodes if needed.");
		ImGui::Text("PruneTree time: %.4fms", stats.pruneTime.count() / static_cast<double>(decltype(stats.pruneTime)::period::den) * 1000);
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Removes empty nodes from the three.");
		ImGui::End();
	}
}
void collSim::visuals::GUIDrawer::CollectStats(const OctreeStats & stats)
{
	auto now = std::chrono::high_resolution_clock::now();
	auto dt = now - lastCollectTime;
	lastCollectTime = now;
	updCooldown -= dt.count() / static_cast<double>(decltype(dt)::period::den);//To secs
	if (updCooldown <= 0.0)
	{
		updCooldown = 0.1;
		this->stats = stats;
	}
}
