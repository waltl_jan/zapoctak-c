#ifndef GL_OCTREEDRAWER_HEADER
#define GL_OCTREEDRAWER_HEADER

#include <glad/glad.h>
#include "Source/GL/Cube.hpp"
#include "Source/GL/Shader.hpp"
#include "Source/CollisionSolvers/OctreeSolver/Octree.hpp"

namespace collSim
{
	class Camera;
	namespace visuals
	{

		//Requires OpenGL context at construction
		class OctreeDrawer
		{
		public:
			//Create Cube, VBO for nodes
			OctreeDrawer(const Camera& cam);
			//For each node add center,halfdist in VBO
			//then draw
			template<typename T>
			void DrawOctree(const Octree<T>& octree);
		private:
			template<typename T>
			void UpdateBuffer(const nodesCollection<T>& nodes);
			//Renders the octree as scaled instanced cubes.
			gl::Shader shader;
			GLuint VAO;
			GLuint cubeVBO;
			GLuint cubeIBO;
			//per-instance data for rendering the octree
			GLuint octreeVBO;
			size_t numInstances;
			//VBO with one Cube
			//VBO with per-instance pos,scale
		};

		template<typename T>
		void OctreeDrawer::DrawOctree(const Octree<T>& octree)
		{
			glBindVertexArray(VAO);
			glBindBuffer(GL_ARRAY_BUFFER, octreeVBO);
			UpdateBuffer(octree.GetNodes());
			shader.Bind();
			//Draw numInstances nodes each defined by 24indicies.
			glDrawElementsInstanced(GL_LINES, 24, GL_UNSIGNED_INT, nullptr, numInstances);
			shader.UnBind();
			glBindVertexArray(0);
		}

		template<typename T>
		void OctreeDrawer::UpdateBuffer(const nodesCollection<T>& nodes)
		{
			size_t numNodes = nodes.Size();
			if (numNodes > numInstances)
			{
				glBufferData(GL_ARRAY_BUFFER, numNodes * 4 * sizeof(float), nullptr, GL_STREAM_DRAW);
			}
			numInstances = numNodes;
			GLenum error = glGetError();
			float* ptr = reinterpret_cast<float*>(glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
			GLenum error2 = glGetError();
			if (ptr != nullptr)
			{
				for (auto&& node : nodes)
				{
					*(ptr++) = static_cast<float>(node.cube.center.x);
					*(ptr++) = static_cast<float>(node.cube.center.y);
					*(ptr++) = static_cast<float>(node.cube.center.z);
					*(ptr++) = static_cast<float>(node.cube.halfDist);
				}
			}
			glUnmapBuffer(GL_ARRAY_BUFFER);

		}
	}
}

#endif