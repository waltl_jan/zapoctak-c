#include "OctreeDrawer.hpp"
#include <string>
#include "Source/GL/Shader.hpp"
#include "Source/Core/Camera.hpp"

namespace collSim
{
	namespace visuals
	{
		namespace
		{
			const std::string vSource = R"(
				#version 330
				layout(location=0) in vec3 cubePos;
				//Per-instance data
				layout(location=1) in vec3 nodePos;
				layout(location=2) in float nodeScale;
				layout(std140) uniform CameraMatrices
				{
					mat4 projection;
					mat4 view;
					mat4 projView;
				} cam;

				void main()
				{
					//Multiply by 2 to match node's halfdist
					gl_Position = cam.projView*vec4(nodePos + nodeScale*2.0*cubePos, 1.0f);
				}	
				)";
			const std::string fSource = R"(
				#version 330
				
				uniform vec4 cubeColor;
				out vec4 color;
				void main()
				{
					color = cubeColor;
				}	
				)";
			constexpr GLfloat cubeVertices[]
			{
				-0.5f, -0.5f, -0.5f,//	Back	Bottom	Left
				+0.5f, -0.5f, -0.5f,//	Back	Bottom	Right
				+0.5f, +0.5f, -0.5f,//	Back	Top		Right
				-0.5f, +0.5f, -0.5f,//	Back	Top		Left
				-0.5f, -0.5f, +0.5f,//	Front	Bottom	Left
				+0.5f, -0.5f, +0.5f,//	Front	Bottom	Right
				+0.5f, +0.5f, +0.5f,//	Front	Top		Right
				-0.5f, +0.5f, +0.5f,//	Front	Top		Left
			};
			constexpr GLuint cubeIndices[]
			{
				0,1,1,2,2,3,3,0,//Back square
				4,5,5,6,6,7,7,4,//Front square
				0,4,1,5,2,6,3,7//Side lines
			};
		}
		OctreeDrawer::OctreeDrawer(const Camera& cam) :
			shader(vSource, fSource)
		{
			shader.Bind();
			shader.SetUniform4f("cubeColor", Vec4f(1.0f, 0.0f, 0.0f, 1.0f));
			shader.UnBind();
			cam.Subscribe(shader);


			glGenVertexArrays(1, &VAO);
			octreeVBO = 0;
			glGenBuffers(1, &cubeIBO);
			glGenBuffers(1, &cubeVBO);
			glGenBuffers(1, &octreeVBO);
			glBindVertexArray(VAO);
			glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 3 * sizeof(GLfloat), 0);
			glEnableVertexAttribArray(0);//cubePos
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeIBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIndices), cubeIndices, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, octreeVBO);
			glVertexAttribPointer(1, 3, GL_FLOAT, 0, 4 * sizeof(GLfloat), 0);
			glEnableVertexAttribArray(1);//Node's position
			glVertexAttribDivisor(1, 1);//Per-instance
			glVertexAttribPointer(2, 1, GL_FLOAT, 0, 4 * sizeof(GLfloat), reinterpret_cast<void*>(3 * sizeof(GLfloat)));
			glEnableVertexAttribArray(2);//Node's size
			glVertexAttribDivisor(2, 1);//Per-instance
			glBindVertexArray(0);

			numInstances = 0;
		}
	}
}