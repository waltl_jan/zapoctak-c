#include "OctreeRenderer.hpp"
#include <External/imgui/imgui.h>
namespace collSim
{
	namespace visuals
	{
		namespace
		{
			const std::string cubeVSource{ R"(
			#version 330
			layout(location=0) in vec3 vecPos;

			layout(std140) uniform CameraMatrices
			{
				mat4 projection;
				mat4 view;
				mat4 projView;
			} cam;
			out vec3 col;

			void main()
			{
				col = vecPos*2.0;
				gl_Position = cam.projView*vec4(2.0*vecPos,1.0f);
			}	
			)" };
			const std::string cubeFSource{ R"(
			#version 330
			
			out vec4 color;
			in vec3 col;
			void main()
			{
				color = vec4(col*0.3f + vec3(0.5f),1.0);
			}	
			)" };
			const std::string sphereVSource{ R"(
			#version 330
			layout(location=0) in vec3 vecPos;


			layout(std140) uniform CameraMatrices
			{
				mat4 projection;
				mat4 view;
				mat4 projView;
			} cam;

			uniform vec3 pos;
			uniform float diameter;

			out vec3 normal;
			out vec3 wPos;
			
			void main()
			{
				vec3 worldPos = vecPos*diameter + pos;
				gl_Position = cam.projView*vec4(worldPos,1.0f);
				wPos=worldPos;
				normal=vecPos;
			}	
			)" };
			const std::string sphereFSource{ R"(
			#version 330

			const vec3 lightPos = vec3(0.0,0.0,0.0);
			const vec3 ambientCol = vec3(0.8,0.8,0.8);
			
			out vec4 color;
			uniform vec3 sColor;
			in vec3 normal;
			in vec3 wPos;
			void main()
			{
				vec3 lightCol = sColor*max(0.0,dot(normal,normalize(lightPos-wPos)));
				color = vec4(0.8*lightCol + 0.2*ambientCol,1.0);
			}	
			)" };
			constexpr Vec3d defCamPos{ -7.0,2.5,7.0 };
		}
		OctreeRenderer::OctreeRenderer(const Octree<Sphere>* octree) :
			graphicsManager("Collision simulation", 1000, 1000, &input), sphereShader(sphereVSource, sphereFSource),
			cubeShader(cubeVSource, cubeFSource), sphere(32), cube(false), octreeDrawer(*graphicsManager.GetCamera()),
			octree(*octree)
		{
			auto& cam = *graphicsManager.GetCamera();
			cam.Subscribe(cubeShader);
			cam.Subscribe(sphereShader);
			//Place camera
			graphicsManager.GetCamera()->LookAt(defCamPos, Vec3d{});
		}
		bool OctreeRenderer::Render()
		{
			if (!graphicsManager.NewFrame())//If window wants to be closed
				return false;

			cubeShader.Bind();
			cube.Draw();

			sphereShader.Bind();

			for (const auto&s : octree.GetObjects())
			{
				sphereShader.SetUniform1f("diameter", static_cast<float>(s.object.radius));
				sphereShader.SetUniform3f("pos", s.object.position);
				sphereShader.SetUniform3f("sColor", s.object.color);
				sphere.Draw();
			}
			sphereShader.UnBind();

			octreeDrawer.DrawOctree(octree);
			guiDrawer.Draw();
			graphicsManager.Render();
			return true;
		}
		void OctreeRenderer::CollectStats(const OctreeStats & stats)
		{
			guiDrawer.CollectStats(stats);
		}
	}
}