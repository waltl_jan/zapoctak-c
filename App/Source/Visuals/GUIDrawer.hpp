#ifndef VISUALS_GUIDRAWER_HEADER
#define VISUALS_GUIDRAWER_HEADER

#include <chrono>
#include "Source/CollisionSolvers/OctreeSolver/Octree.hpp"
namespace collSim
{
	namespace visuals
	{
		class GUIDrawer
		{
		public:
			GUIDrawer();
			void Draw();
			void CollectStats(const OctreeStats& stats);
		private:
			std::chrono::high_resolution_clock::time_point lastCollectTime ;
			double updCooldown;
			OctreeStats stats;
		};
	}
}

#endif