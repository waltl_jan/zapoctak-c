#ifndef VISUALS_RENDERED_HEADER
#define VISUALS_RENDERED_HEADER

namespace collSim
{
	namespace visuals
	{
		class Renderer
		{
		public:
			//Returns false if App wants to quit
			virtual bool Render() = 0;
		};
	}
}
#endif