
#ifndef VISUALS_OCTREERENDERER_HEADER
#define VISUALS_OCTREERENDERER_HEADER

#include "Renderer.hpp"
#include "Source/Math/Math.hpp"
#include "Source/Core/GraphicsManager.hpp"
#include "Source/GL/Shader.hpp"
#include "Source/GL/Cube.hpp"
#include "Source/GL/Sphere.hpp"
#include "Source/Core/Input.hpp"
#include "Source/CollisionSolvers/OctreeSolver.hpp"
#include "OctreeDrawer.hpp"
#include "GUIDrawer.hpp"

namespace collSim
{
	struct OctreeStats;
	namespace visuals
	{
		class OctreeRenderer :public Renderer
		{
		public:
			OctreeRenderer(const Octree<Sphere>* octree);
			virtual bool Render() override;
			void CollectStats(const OctreeStats& stats);
		private:
			//ORDER DEPENDENT
			Input input;
			GraphicsManager graphicsManager;
			gl::Shader sphereShader;
			gl::Shader cubeShader;
			gl::Cube cube;
			gl::Sphere sphere;
			OctreeDrawer octreeDrawer;
			GUIDrawer guiDrawer;
			const Octree<Sphere>& octree;
		};
	}
}

#endif