#include "Cube.hpp"

#include <vector>
#include <algorithm>
#include "Source/Math/Math.hpp"
namespace collSim
{
	namespace gl
	{
		Cube::Cube(bool faceOutwards/*=true*/)
		{
			GLfloat vertices[]
			{
				-0.5f, -0.5f, -0.5f,//	Back	Bottom	Left
				+0.5f, -0.5f, -0.5f,//	Back	Bottom	Right
				+0.5f, +0.5f, -0.5f,//	Back	Top		Right
				-0.5f, +0.5f, -0.5f,//	Back	Top		Left
				-0.5f, -0.5f, +0.5f,//	Front	Bottom	Left
				+0.5f, -0.5f, +0.5f,//	Front	Bottom	Right
				+0.5f, +0.5f, +0.5f,//	Front	Top		Right
				-0.5f, +0.5f, +0.5f,//	Front	Top		Left
			};
			std::vector<GLuint> indices
			{
				0,3,2,2,1,0,//	Back
				4,5,6,6,7,4,//	Front
				7,3,0,0,4,7,//	Left
				6,5,1,1,2,6,//	Right
				0,1,5,5,4,0,//	Bottom
				3,7,6,6,2,3,//	Top
			};
			if (!faceOutwards)
				std::reverse(indices.begin(), indices.end());

			glGenVertexArrays(1, &VAO);
			glGenBuffers(1, &VBO);
			glGenBuffers(1, &IBO);

			glBindVertexArray(VAO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, 0, 3 * sizeof(GLfloat), 0);
			glEnableVertexAttribArray(0);

			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, 36 * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);
			glBindVertexArray(0);
		}
		Cube::~Cube()
		{
			glDeleteBuffers(1, &VBO);
			glDeleteBuffers(1, &IBO);
			glDeleteVertexArrays(1, &VAO);
		}
		void Cube::Draw()
		{
			glBindVertexArray(VAO);
			glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
		}
	}
}
