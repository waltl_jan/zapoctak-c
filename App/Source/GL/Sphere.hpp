#ifndef OPENGL_SPHERE_234724901641_HEADER
#define OPENGL_SPHERE_234724901641_HEADER

#include <cstddef>
#include <glad/glad.h>

namespace collSim
{
	namespace gl
	{
		//VAO buffer containing centered unit sphere of given resolution = number of rings
		//Requires OpenGL context at construction
		class Sphere
		{
		public:
			Sphere(size_t resolution);
			~Sphere();
			//Needs appropriate shader with following inputs:
			//location=0 vec3 **position**
			//Positions also act as normals
			void Draw();
		private:
			GLuint VBO, VAO, IBO;
			size_t numIndices;
		};
	}
}


#endif
