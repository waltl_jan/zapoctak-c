#ifndef COLLSIM_CORE_SHADER_HEADER
#define COLLSIM_CORE_SHADER_HEADER

#include <string>
#include <glad/glad.h>
#include <unordered_map>
#include "Source/Math/Math.hpp"

namespace collSim
{
	namespace gl
	{
	//Wrapper around OpenGL's vertex&fragment shaders
	//Requires OpenGL context at construction
		class Shader
		{
		public:
			Shader(const std::string& vertexSource, const std::string& fragSource);
			Shader(const Shader&) = delete;
			Shader(Shader&&) = default;
			Shader& operator=(const Shader&) = delete;
			Shader& operator=(Shader&&) = default;
			~Shader();

			void Bind() const;
			void UnBind() const;

			void SetUniform1f(const std::string& name, float x);
			void SetUniform1i(const std::string& name, int i);
			void SetUniform2f(const std::string& name, float x, float y);
			//Still sets vec2 of floats, just less casting
			void SetUniform2f(const std::string& name, const Vec2d& vec);
			void SetUniform2f(const std::string& name, const Vec2f& vec);
			void SetUniform3f(const std::string& name, float x, float y, float z);
			//Still sets vec3 of floats, just less casting
			void SetUniform3f(const std::string& name, const Vec3d& vec);
			void SetUniform3f(const std::string& name, const Vec3f& vec);
			void SetUniform4f(const std::string& name, float x, float y, float z, float w);
			//Still sets vec4 of floats, just less casting
			void SetUniform4f(const std::string& name, const Vec4d& vec);
			void SetUniform4f(const std::string& name, const Vec4f& vec);
			void SetUniform4Mat(const std::string& name, const Mat4f& mat);

			void SetUniformBlockBinding(const std::string& blockName, GLuint bindingPoint);
		private:
			GLuint programID;
			//Obtains locations of shader's uniforms and stores them in the map
			void LoadUniforms();
			// Map that holds all uniforms and their locations
			std::unordered_map<std::string, GLint> uniforms;
		};
	}
}

#endif