#ifndef OPENGL_CUBE_523418971_HEADER
#define OPENGL_CUBE_523418971_HEADER

#include <glad/glad.h>

namespace collSim
{
	namespace gl
	{
		//VAO that contains a center unit cube
		class Cube
		{
		public:
			//Whether faces are oriented outward or inwards
			Cube(bool faceOutwards = true);
			~Cube();
			//Require appropriate Shader to be bounded with following inputs:
			//location=0 vec3 **positions**
			void Draw();
		private:
			GLuint VAO, VBO, IBO;
		};
	}
}
#endif