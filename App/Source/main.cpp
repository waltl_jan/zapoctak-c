#include <iostream>
#include <string>
#include "Core/Exception.hpp"
#include "Core/Simulation.hpp"
#include "CollisionSolvers/OctreeSolver.hpp"
#include "Visuals/OctreeRenderer.hpp"

int main(int argc, char**argv)
{
	using namespace collSim;
	try
	{
		if (argc < 3)
			throw Exception("Program expects 4 arguments - number of objects(int), split limit for nodes(int), cubeCollision(true/false).\n"
				"Example: program.exe 500 5 true\n");

		std::size_t numObjects = std::stoi(argv[1]);
		std::size_t splitLimit = std::stoi(argv[2]);
		bool cubeColl = (argc < 4 || argv[3] == "true") ? true : false;
		double minSize = argc >= 5 ? std::stoi(argv[4]) : 0.005;
		double maxSize = argc >= 6 ? std::stoi(argv[6]) : 0.005;
		Simulation sim{};
		OctreeSolver solver{ numObjects,splitLimit,minSize,maxSize,cubeColl };
		visuals::OctreeRenderer octreeRenderer(&solver.GetTree());
		solver.SetStatsCallback([&octreeRenderer](const OctreeStats& stats) { octreeRenderer.CollectStats(stats); });
		sim.Start(solver, octreeRenderer);
	}
	catch (const std::exception& e)
	{
		std::cout << "Following exception has been thrown:\n " << e.what() << '\n';
		system("PAUSE");
	}
	return 0;
}