cmake_minimum_required(VERSION 3.6)

project(Zapoctak)

#set output 
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

#only compile the library
set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
add_subdirectory(glfw-3.2.1)

#Make Source/ and libraries(GLFW,ImGui,GLAD) available
include_directories(App/ App/External glfw-3.2.1/include)



add_executable (App "App/Source/main.cpp")

target_link_libraries(App glfw)
# use c++14 for lambdas, variadic templates...
set_property(TARGET App PROPERTY CXX_STANDARD 17)
set_property(TARGET App PROPERTY CXX_STANDARD_REQUIRED ON)
set_property(TARGET App PROPERTY USE_FOLDERS ON)

#######################APP EXTERNAL SOURCE FILES###############################

set(IMGUI_SOURCES 
	"App/External/imgui/imconfig.h"
	"App/External/imgui/imgui.cpp"
	"App/External/imgui/imgui.h"
	"App/External/imgui/imgui_demo.cpp"
	"App/External/imgui/imgui_draw.cpp"
	"App/External/imgui/imgui_internal.h"
	"App/External/imgui/stb_rect_pack.h"
	"App/External/imgui/stb_textedit.h"
	"App/External/imgui/stb_truetype.h"
)
source_group("External\\imgui\\" FILES ${IMGUI_SOURCES})
target_sources(App PUBLIC ${IMGUI_SOURCES})

set(GLAD_SOURCES 
	"App/External/glad/glad.h"
	"App/External/glad/glad.c"
)
source_group("External\\glad\\" FILES ${GLAD_SOURCES})
target_sources(App PUBLIC ${GLAD_SOURCES})

#######################APP SOURCE FILES########################################
source_group("Source\\" FILES "App/Source/main.cpp")

set(APP_CORE_SOURCES
	"App/Source/Core/Camera.hpp"
	"App/Source/Core/Camera.cpp"
	"App/Source/Core/Exception.hpp"
	"App/Source/Core/GraphicsManager.hpp"
	"App/Source/Core/GraphicsManager.cpp"
	"App/Source/Core/ImGuiBackend.hpp"
	"App/Source/Core/ImGuiBackend.cpp"
	"App/Source/Core/Key.hpp"
	"App/Source/Core/Input.hpp"
	"App/Source/Core/Simulation.hpp"
	"App/Source/Core/Simulation.cpp"
)
source_group("Source\\Core\\" FILES ${APP_CORE_SOURCES})
target_sources(App PUBLIC ${APP_CORE_SOURCES})

set(APP_GL_SOURCES
	"App/Source/GL/Cube.hpp"
	"App/Source/GL/Cube.cpp"
	"App/Source/GL/GLError.hpp"
	"App/Source/GL/GLError.cpp"
	"App/Source/GL/Shader.hpp"
	"App/Source/GL/Shader.cpp"
	"App/Source/GL/Sphere.hpp"
	"App/Source/GL/Sphere.cpp"
)
source_group("Source\\GL\\" FILES ${APP_GL_SOURCES})
target_sources(App PUBLIC ${APP_GL_SOURCES})

set(APP_MATH_SOURCES
	"App/Source/Math/Common.hpp"
	"App/Source/Math/Math.hpp"
	"App/Source/Math/Mat4.hpp"
	"App/Source/Math/Vec2.hpp"
	"App/Source/Math/Vec3.hpp"
	"App/Source/Math/Vec4.hpp"
)
source_group("Source\\Math\\" FILES ${APP_MATH_SOURCES})
target_sources(App PUBLIC ${APP_MATH_SOURCES})

set(APP_SOLVERS_SOURCES
	"App/Source/CollisionSolvers/CollisionSolver.hpp"
	"App/Source/CollisionSolvers/OctreeSolver.hpp"
	"App/Source/CollisionSolvers/OctreeSolver.cpp"
)
source_group("Source\\CollisionSolvers\\" FILES ${APP_SOLVERS_SOURCES})
target_sources(App PUBLIC ${APP_SOLVERS_SOURCES})

set(APP_OCTREE_SOLVER_SOURCES
	"App/Source/CollisionSolvers/OctreeSolver/CommonTypes.hpp"
	"App/Source/CollisionSolvers/OctreeSolver/Node.hpp"
	"App/Source/CollisionSolvers/OctreeSolver/Octree.hpp"
	"App/Source/CollisionSolvers/OctreeSolver/OctreeCollection.hpp"
)
source_group("Source\\CollisionSolvers\\OctreeSolver\\" FILES ${APP_OCTREE_SOLVER_SOURCES})
target_sources(App PUBLIC ${APP_OCTREE_SOLVER_SOURCES})

set(APP_GEOMETRY_SOURCES
	"App/Source/Geometry/Sphere.hpp"
	"App/Source/Geometry/AABB.hpp"
)
source_group("Source\\Geometry\\" FILES ${APP_GEOMETRY_SOURCES})
target_sources(App PUBLIC ${APP_GEOMETRY_SOURCES})

set(APP_VISUALS_SOURCES
	"App/Source/Visuals/Renderer.hpp"
	"App/Source/Visuals/OctreeRenderer.hpp"
	"App/Source/Visuals/OctreeRenderer.cpp"
	"App/Source/Visuals/OctreeDrawer.hpp"
	"App/Source/Visuals/OctreeDrawer.cpp"
	"App/Source/Visuals/GUIDrawer.hpp"
	"App/Source/Visuals/GUIDrawer.cpp"
)

source_group("Source\\Visuals\\" FILES ${APP_VISUALS_SOURCES})
target_sources(App PUBLIC ${APP_VISUALS_SOURCES})
###############################################################################

#Copy examples and documentation to same folder
#add_custom_command(TARGET App POST_BUILD
#                   COMMAND ${CMAKE_COMMAND} -E copy_directory
#                       ${CMAKE_SOURCE_DIR}/Data ${CMAKE_BINARY_DIR}/bin)
#add_custom_command(TARGET App POST_BUILD
#                   COMMAND ${CMAKE_COMMAND} -E copy
#                       ${CMAKE_SOURCE_DIR}/ZimniDokumentace/main.pdf ${CMAKE_BINARY_DIR}/bin/ZimniDokumentace.pdf)
#add_custom_command(TARGET App POST_BUILD
#                   COMMAND ${CMAKE_COMMAND} -E copy
#                       ${CMAKE_SOURCE_DIR}/LetniDokumentace/main.pdf ${CMAKE_BINARY_DIR}/bin/LetniDokumentace.pdf)


