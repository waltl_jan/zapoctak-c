﻿# Instalace
Program by mělo jít zkompilovat pomocí cmake a make:

    cd <git root>
    mkdir build && cd build/
    cmake -DCMAKE_BUILD_TYPE=Release .. && make
Pokud si CMake bude stěžovat, tak nejspíše chybí  balíček X11 - potřebuje ho GLFW(Grafická knihovna), což se dá kdyžtak doinstalovat přes

    sudo apt-get install xorg-dev
Vyžaduje OpenGL 3.3, ale to by snad mělo být OK.
# Tutoriál
Program se spustí okno, které ukazuje kostku s koulemi a také vykresluje jednotlivé uzly stromu. Scéna se dá ovládát myší a kolečkem jako trackball. Vlevo je okno se statistikami (dá se zvětšovat). Program očekává na vstupu minimálně 2 argumenty:

    ./a.out x y [b=true] [min=0.01] [max=0.01]

`x=int` je počet simulovaných objektů(koulí) v octree. `y=int` udává maximální počet objektů, který by každý uzel ve stromě měl obsahovat. `b=true/false` zapíná/vypíná kolizi s kostkou, ve které jsou simulované koule. Octree je dynamický, což se projeví právě vypnutím kostky, ale je to pak celkem nezajímavé. `min,max` určují poloměry koulí, kostka má hranu o délce 2, takže je potřeba volit rozumně s ohledem na počet koulí.

Na mém notebooku běží následující program plynule, ale je to na hraně. (Dost možná je pak limitem i renderování, protože není extra efektivní). Na druhou stranu N^2 algoritmus nezvládne ani 500 objektů. 

	./a.out 10000 4 true 0.005 0.005

Octree funguje tak, že se snaží každý objekt umístit co nejhlouběji do stromu. To znamená umístit ho do co nejmenšího uzlu, kterýho ho obsahuje. Každý simulační krok nejdříve všechny objekty posune dle svých rychlostí. Octree pak zavolá metodu *UpdateTree*, která pro každý objekt přepočítá jeho pozici ve stromě - pokud v důsledku posunutí v prostoru vypadl ze svého uzlu, tak jej přesune na správné místo. Přitom vytváří nové uzly - pokud se nějaký naplní(limit `y`), popřípadě rozšíří kořen aby strom obsahoval všechny objekty. Prázdné uzly odstraňuje metoda *PruneTree*. Rychlost obou metod je měřena a je ve statistikách. Nastavený limit je pouze doporučený, neboť se může stát, že žádný objekt v naplněném uzlu nelze přesunout do žádného syna, což se stane když jsou objekty na osách uzlu. Dal jsem přednost tomuto řešení místo toho, že by se umístily do více synů, protože se pak špatně přesouvají. Rychlost těchto metod je měřena a uvedena ve statistice.

Po opravě stromu se na každý objekt *A* zavolá metoda *QueryTree*, která najde první objekt se kterým *A* koliduje. V případě dobře postaveného stromu je tato funkce výrazně rychlejší než testování všech objektů. Může se stát, že *A* koliduje s více objekty, ale to se stejně nedá obecně rozumě vyřešit, čili v jednom kroku se vždy řeší jen jedna kolize a zbytek se případně nechá na další simulační kroky. *QueryTree* prochází strom od kořene a zanořuje se do uzlů se kterými se *A* protíná, v nich otestuje všechny objekty a pokračuje dál. Celkový počet těchto testů je uveden ve statistice. Při správně nastaveném limitu je  tento počet výrazně menší než N^2 a overhead z *UpdateTree, PruneTree*  je mnohem menší než ušetřený čas za netestování kolizí.   Což se dá lehce vyzkoušet tím, že nastavíme `y>x` a tím dostaneme naivní řešení kolizí v N^2. 

# Dokumentace

**Základ:**

 - **Core/Simulation** - Hlavní třída programu, která obsahuje while(true) smyčku se simulací (**OctreeSolver**), časování a renderováním(**OctreeRenderer**).
 - **Core/GraphicsManager** - Stará se o grafickou stránku programu = inicializuje GLFW, OpenGL a ImGui( skrz **Core/ImGuiManager**).
 - **Geometry/Sphere** - Simulovaný objekt, jediný důvod proč je move-only je ukázat, že to Octree zvládne.
 - **Visuals/** - Třídy starající se o vykreslování v OpenGL, celkem nezajímavé.
 
**Octree část:**
Nachází se ve složce **CollisionSolvers** a taky uvnitř **OctreeSolver**
 - **OctreeSolver** - Obsahuje třídu **Octree\<Sphere\>** o kterou se stará.
 - **Octree** - Asi nejzajímavější třída, která implementuje samotný strom. Je obecně parametrizovatelná typem objektu, které bude ukládat. Strom je reprezentovaný pomocí propojených tříd **Node**, které jsou uloženy lineárně. Samotné objekty si drží zabalené v **ObjectWrapper\<Objekt\>** kvůli tomu, aby objekt věděl do kterého uzlu patří. Tyto wrappery jsou také lineárně uloženy. Na oboje je použita třída **OctreeCollection**.
- **OctreeCollection** je wrapper okolo *std::vector*, který dokáže přidávát, odebírat vrcholy skoro bez zneplatnění iterátorů. Iterátory nepoužívají pointery, ale indexy, což řeší relokaci vektoru při přidávání. Odebrání objektu *A* ho prohodí  s posledním objektem *B* ve vektoru a následně objekt *A* popne. Dojde tedy pouze k zneplatnění iterátorů na objekt *B*, což se řeší callbackem, který může iterátory opravit. Což právě využívá **Octree** neboť o všech iterátorech na objekt *B* ví.
- **Node** je uzel stromu, který si pamatuje svého rodiče, své děti(těch je až 8) a seznam objektů, které v něm jsou. 


			 

